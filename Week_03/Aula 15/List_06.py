#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 30 14:40:04 2020

@author: filipeteto
"""
#------------------------------ Ficha 06 ------------------------------------#


#---------------------------- Exercício 01 ----------------------------------#

def circle_data():

    from math import pi
    radius = float(input ("Input the radius of the circle : "))
    print ("The area of the circle with radius " + str(radius) + " is: " + str(pi * radius**2))
    print ("The perimeter of the circle with radius " + str(radius) + " is: "+ str(2*radius*3.14))


circle_data()

#---------------------------- Exercício 02 ----------------------------------#



def square_data():
    square_length = float(input("Input the length of a side from the square: "))
    print("The perimeter of the square is: " + str(square_length*4))
    print("The area of the square is: "+ str(square_length**2))
    

square_data()


#---------------------------- Exercício 03 ----------------------------------#

def square_n_circle():
    
    circle_data()
    square_data()

square_n_circle()


#---------------------------- Exercício 04 ----------------------------------#

from math import pi
from math import sqrt
def find_area():
    
    radius= input("Insert radius: ")
    
    s = sqrt(2)
    sidesquare = (s*((radius*2)/2))
    square = ((sidesquare*2)**2)
    circle = ((pi*radius)**2)
    area = circle-square
    print(("The area of the biggest square inside the circle with the radius\
           you gave is: "), area)
find_area()









