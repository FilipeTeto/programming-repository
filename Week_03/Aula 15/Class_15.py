#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 30 17:28:12 2020

@author: filipeteto
"""

#-------------------------------- Aula 15 -----------------------------------#

# Classes

class house:
    
    area = 120              # variáveis da classe
    window_size = (2,3)     # variáveis da classe
    
    def abre_porta(self):         # funções da classe tem de conter sempre (self)
        pass                # pass indica que há código em falta e o python irá ignorar
    
    # variáveis e funções dentro de uma classe são chamados de atributos
    # tudo o que está dentro de uma classe são atributos     
    
    def __init__(self,cor_das_paredes=0x000000, cor_do_telhado=0x0000ff):
        self.cor_das_paredes = cor_das_paredes
        self.cor_do_telhado = cor_do_telhado
        
        
    
print(house.area)        # usa notação ponto para aceder a uma propriedade da classe
# print(abre_porta.func())    # funções da classe só podem ser acedidas por objectos da classe
                            # a menos que se consiga passar um objecto  

# """
# int()    # cria um inteiro   São chamados de inicializadores
# float()  # cria um float

# """
# concrete_house() = house()cria um objecto com os atributos definidos na classe criada

concrete_house = house(0xffffff, 0x0000ff) # 0x indica que número é hexadecimal
concrete_house.area = 450

print(concrete_house.cor_das_paredes)
print(concrete_house.cor_do_telhado)

another_house = house()
another_house.cor_do_telhado
another_house.cor_das_paredes
another_house.portas = 2
print("Quantas portas? " + str(another_house.portas))
# del(another_house.portas)                                   # apaga atributo do objecto da classe
# print("Quantas portas? " + str(another_house.portas))

and_another_house = house()
print(and_another_house.area)


# Herança de classes
class Pais:
    
    cabelo = "castanho"
    
    def __init__(self, nome, idade):
        self.nome = nome
        self.idade = idade
    
    def trabalhar(self):
        print("Trabalhar")
        
class human:
    compassion = True
        
class eu(Pais,human):
    
    humor = "mau"
    def __init__(self, nome, idade,hobby):
        super().__init__(nome,idade) # serve para ter os atributos do init da função superior
        self.hobby = hobby
    pass
    
    def trabalhar(self):
        super().trabalhar()
        print("e trabalhar mais.")

eu = eu("Filipe",29,"Jogar à bola")

print(eu.nome)
print(eu.idade)
print(eu.cabelo)
print(eu.humor)
print(eu.hobby)
eu.trabalhar()
print("I have compassion:",eu.compassion)


# class estrutura_de_dados:
#     pass

# estrutura = estrutura_de_dados()
# estrutura.x = 2



