#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 30 08:41:26 2020

@author: filipeteto
"""

#---------------------------------- Ficha 04 --------------------------------#

#-------------------------------- Exercício 01 ------------------------------#

# Suponha que executava os seguintes programas. Qual seria o resultado?
"""
# 1.
out_file = open(“outFile.txt”, 'w') # irá escrever no ficheiro  números de 1 a 99
for i in range(100):                # caso exista um ficheiro com o mesmo nome, será apagado.
out_file.write(str(i))

# 2.
in_file = open(“inFile.txt”,'r') #irá ler o ficheiro na totalidade e fechar o ficheiro
indata = in_file.read()          # não irá abrir a segunda vez
in_file.close()
indata = in_file.read()
in_file.close()

# 3.
in_file = open(“inFile.txt”,'r')    # programa irá ler linha a linha
print(in_file.readline())
in_file = open(“inFile.txt” ,'r'))
print(in_file.readline())
in_file.close()
"""
#-------------------------------- Exercício 02 ------------------------------#
#
with open("ficha04.txt") as file_reader:
    for line in file_reader:
        print(line)

#-------------------------------- Exercício 03 ------------------------------#

line_number = 0
with open("ficha04.txt") as file_reader:
    for line in file_reader:
        line_number += 1
        print(line_number, line)

#-------------------------------- Exercício 04 ------------------------------#
"""
Dados referentes a observações são frequentemente guardados em ficheiros de texto. Por
exemplo, as temperaturas lidas a várias horas do dia, ao longo de vários dias, podem ser
guardadas num ficheiro de números em vírgula flutuante, onde cada linha contém os valores
das várias temperaturas medidas num dia.
"""
from statistics import mean

def medias(ficheiro):
    try:
        with open(ficheiro) as file_reader:
            for line in file_reader:
                line = line.strip()
                split_line = line.split()
                # for i in range(len(split_line)):
                    #     split_line[i] = float(split_line[i])
                float_list = list(map(float,split_line))
                print(round(mean(float_list),2))
                
                medias("temperaturas.txt")
    except:
          print("Somethin went wrong while reading the file.")
    
"""
Utilizando a função media, escreva uma função médias que, dado o nome de um ficheiro de
texto como o acima, imprima as temperaturas médias diárias. Deverá imprimir um valor por
linha e tantos valores quantas as linhas do ficheiro. Sugestão: utilize o método string.split(s)
para obter a lista de palavras existentes numa string. A função media deve apanhar a
exceções referentes à utilização do ficheiro. Para isso utilize um try: finally: ou um with,
para se assegurar que fecha o ficheiro. Exceções sobre a abertura do ficheiro deverão ser
tratadas pelo chamador (ver exercício seguinte).
"""

#-------------------------------- Exercício 05 ------------------------------#

def ficheiro_falha():
    user_input = input("Indique o nome do ficheiro:")
    try:
        medias(user_input)
    except:
        print("Erro de I/O ao ler o ficheiro")
        
        
#-------------------------------- Exercício 06 ------------------------------#
def linha_para_elemento(line):
    line = line.split()
    dict1 = {"name":line[0],"atomic":line[1],"density":line[2]}
    return dict1
with open("/Users/filipeteto/Desktop/programming-repository/Week_03/Aula 14/elementos.txt", 'r') as elementos:
    lista = []    
    for line in elementos:
        
        lista.append(linha_para_elemento(line))
print(lista)


#-------------------------------- Exercício 07 ------------------------------#

def escrever_elementos(nome_ficheiro,lista):
    with open(nome_ficheiro,"w") as ficheiro:
        for elemento in lista:
            ficheiro.write(elemento["name"]+" ")
            ficheiro.write(str(elemento["atomic"])+" ")
            ficheiro.write(str(elemento["density"])+"\n")
nome_ficheiro=input("Insira um nome para o ficheiro: ")
nome_ficheiro=nome_ficheiro + ".txt"
escrever_elementos(nome_ficheiro,lista)


#-------------------------------- Exercício 08 ------------------------------#

def linha_para_atomo(line):
    line = line.split()
    if "ATOM" in line[0]:
            dict1 = {"símbolo":line[2],"id":line[1],"x":line[3],"y":line[4],"z":line[5]}
            return dict1
    else:
        pass
            
lista=[]
with open("/Users/filipeteto/Desktop/programming-repository/Week_03/Aula 14/atomos.txt", 'r') as atomos:
                for line in atomos:
                    if linha_para_atomo(line)!=None:
                        lista.append(linha_para_atomo(line))
                    else:
                        continue

print(lista)

#------------- CSV --------------- Exercício 01 ------------------------------#

# import csv
# with open('eggs.csv', newline='') as csvfile:
#      spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
#      for row in spamreader:
#          print(', '.join(row))
 
"""
   
Comma Separated Values (CSV) é o formato mais comum de importação/exportação de
dados para folhas de cálculo e para bases de dados. A linguagem Python conta com um
módulo para o efeito, CSV File Reading and Writing. O padrão mais comum de utilização do
CSV para leitura é o seguinte:
"""
import csv
with open('ficheiro.csv', 'rU') as ficheiro_csv:
    leitor = csv.reader(ficheiro_csv, delimiter=';')
# # No código acima leitor é um iterador. Pode ser usado num ciclo for, como por exemplo:
#     for linha in leitor:
# <fazer algo com linha>

# """
# Alternativamente, podemos chamar o método leitor.next() para obter os sucessivos
# elementos no iterador. A exceção StopIteration é levandada quando tentamos fazer um
# next() e não há mais elementos no iterador. A razão para usar 'rU' está relacionado com a
# função open(), e não propriamente com CSV. O parâmetro 'U' (de Universal), aceita linhas
# com a convenção Unix ('\n') e Windows ('\r\n'). O delimitador ';' é importante pois o formato
# CVS para a língua portuguesa utiliza o ponto-e-vírgula como separador, uma vez que a
# vírgula é utilizada como separador da parte decimal dos números. Escreva uma função
# csvLinhaParaGrafico que leia dum ficheiro CSV o gráfico de uma função e o devolva. A
# função recebe o nome de um ficheiro. Assuma que o ficheiro é composto por duas linhas, a
# primeira contendo os valores das abcissas, a segunda os valores das ordenadas. Não se
# esqueça de converter as strings lidas do ficheiro em números em vírgula flutuante.
#     """
import csv
def le_graficos(ficheiro_csv):
    grafico = []
    with open(ficheiro_csv) as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=";", quotechar='|')
        linha1 = csv_reader.__next__()
        linha2 = csv_reader.__next__()
        for i in range(len(linha1)):
            item_linha1_com_ponto = linha1[i].replace(',','.')
            item_linha2_com_ponto = linha2[i].replace(',','.')
            grafico.append((float(item_linha1_com_ponto), float(item_linha2_com_ponto)))
    return grafico
print(le_graficos("graficos.csv"))            
            
            
def converte_para_notacao_ponto(item1,item2):
    return float(item1.replace(',','.')),float(item2.replace(',','.'))
def le_graficos2(ficheiro_csv):
    grafico = []
    with open(ficheiro_csv) as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=";", quotechar='|')
        linha1 = csv_reader.__next__()
        linha2 = csv_reader.__next__()
        grafico = list(map(converte_para_notacao_ponto,linha1,linha2))
    return grafico
print(le_graficos2("graficos.csv"))               
            
            
#------------- CSV --------------- Exercício 02 ------------------------------#

import csv
def grafico_para_csv_linha(nome_ficheiro,grafico):
    with open(nome_ficheiro, "w", newline="") as ficheiro_csv:
        escritor = csv.writer(ficheiro_csv)
        escritor.writerow(grafico[0])
        escritor.writerow(grafico[1])
nome_ficheiro = input("Indique um nome para o ficheiro: ")
nome_ficheiro =nome_ficheiro+".csv"
grafico = [[1,3,5,7],[2,4,6,8]]
grafico_para_csv_linha(nome_ficheiro,grafico)


#------------- CSV --------------- Exercício 03 ------------------------------#

import csv
def graficoParaCsv(nome_ficheiro,grafico):
    with open(nome_ficheiro,"w",newline="") as ficheiro:
        x = False
        escritor = csv.writer(ficheiro)
        for graficos in lista_graficos:
            if x==False:
                escritor.writerow(graficos[0])
                x=True
            else:
                pass
            escritor.writerow(graficos[1])
lista_graficos = [[1,2],[3,4],[1,2],[5,6],[1,2],[7,8]]
nome_ficheiro= input("Insira um nome para o ficheiro: ")
nome_ficheiro=nome_ficheiro+".csv"
graficoParaCsv(nome_ficheiro,lista_graficos)

