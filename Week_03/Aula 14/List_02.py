#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 28 16:22:31 2020

@author: filipeteto
"""

#--------------------------- Ficha de Exercícios 02 -------------------------#


#------------------------------- Exercício 01 -------------------------------#

# Considere a seguinte função:
    
def imprimeDivisaoInteira(x,y):
    """
    Divide um inteiro por outro

    Parameters
    ----------
    x : DIVIDENDO
    y : DIVISOR

    Returns
    Resultado da divisão inteira de x por y
    ----------
    

    """
    if y == 0:
        print("Divisão por zero")
    else:
        print(x//y)
        
        
#-------------------------------A)
"""
Faz a divisão de dois números arredondada.
"""


#-------------------------------B)

imprimeDivisaoInteira(4, 2) # 2
imprimeDivisaoInteira(2, 4) # 0
imprimeDivisaoInteira(3, 0) # Divisão por zero
help(imprimeDivisaoInteira) # Imprime o docstring
# imprimeDivisaoInteira()   # não faz nada pois não tem argumentos


#-------------------------------C)



#------------------------------- Exercício 02 -------------------------------#

def potenciaP(i):    
    return i**i

def potencia(i,j = None):
    return i**i if j == None else i**j
    
print(potencia(2))
print(potencia(2,0))

#------------------------------- Exercício 03 -------------------------------#

a = 4
def printFuncao():
    a = 17
    print("Dentro da funcao: ", a)
printFuncao()   
print("Fora da funcao: ", a )

#------------------------------- Exemplo 03 ---------------------------------#

def function1():
    a= 3
    print("Non local:", a)
    def function2():
        a= 4
        print("Local: ", a)
    function2()
function1()

#------------------------------- Exemplo 04 ---------------------------------#

# Considere o seguinte código:
    
# def somaDivisores(num):
# 
# Soma de divisores de um numero dado
# Requires:
# num seja int e num > 0
# Ensures: um int correspondente à soma dos divisores
# de num que sejam maiores que 1 e menores que num
# 
"""
a. Como cliente da função somaDivisores, o que deve satisfazer para a função cumprir oontrato?
 ->  O número deverá ser maior que 0 e ser inteiro.
 
b. E o que obtém da função se a chamar satisfazendo a sua pré-condição?
 ->  Um número inteiro correspondente à soma dos divisores de um número que sejam
     maiores que 1 e menores que esse número.
"""

#------------------------------- Exemplo 05 ---------------------------------#

def SomaDivisores(num):
    divisors = [1]
    for i in range(2, num):
        if (num % i)==0:
            # se o resto for igual a 0
            divisors.append(i)
    return sum(divisors)

while True:
    num = int(input("Escreva um número inteiro positivo:"))
    print('A soma dos divisores é: ', SomaDivisores(num))
    if num <= -1:
        print("O número deve ser positivo.")
        break
#------------------------------- Exercício 06 -------------------------------#

DIA_ATUAL = 29
MES_ATUAL = 10
ANO_ATUAL = 2020
# print ("Dados do Pai")
# anoPai = int(input("Introduza o ano de nascimento: "))
# mesPai = int(input("Introduza o mes de nascimento: "))
# diaPai = int(input("Introduza o dia de nascimento: "))
# print ("Dados da Mae")
# anoMae = int(input("Introduza o ano de nascimento: "))
# mesMae = int(input("Introduza o mes de nascimento: "))
# diaMae = int(input("Introduza o dia de nascimento: "))
# if mesPai > MES_ATUAL or \
# (mesPai == MES_ATUAL and diaPai > DIA_ATUAL):
#     print("Pai tem", ANO_ATUAL - anoPai - 1, "ano(s)")
# else:
#     print("Pai tem", ANO_ATUAL - anoPai, "ano(s)")
# if mesMae > MES_ATUAL or \
# (mesMae == MES_ATUAL and diaMae > DIA_ATUAL):
#     print("Mae tem", ANO_ATUAL - anoMae - 1, "ano(s)")
# else:
#     print("Mae tem", ANO_ATUAL - anoMae, "ano(s)")


def askData(i):
    year_asked = int(input("Introduza o ano de nascimento: "))
    month_asked = int(input("Introduza o mes de nascimento: "))
    day_asked = int(input("Introduza o dia de nascimento: "))
    print(i,'nasceu a ',day_asked,month_asked,year_asked,'.') 
    if month_asked > MES_ATUAL or \
            (month_asked == MES_ATUAL and day_asked > DIA_ATUAL):
                print(i, "tem", ANO_ATUAL - year_asked - 1, "ano(s)")
    else:
            print(i, "tem", ANO_ATUAL - year_asked, "ano(s)")
askData("Mãe")
askData("Pai")
# #------------------------------ Exercício 07 ------------------------------#

"""

Escreva uma função que receba dois números inteiros e devolva o maior deles. Inclua o
contrato da função. Teste a função escrevendo um programa que receba dois números
inteiros do utilizador e imprima o resultado da chamada à função desenvolvida. Como teria
de fazer para determinar o menor de dois números com uma segunda função que tirasse
partido de chamar a primeira?

"""

def maximum(a,b):
    """
    

    Parameters
    ----------
    a : int.
    
    b : int.

    Returns
    -------
    int: The maximum number between two integers.

    """
    # return a if a > b else b
    return max(a,b)
    
print(maximum(2,16))    

def minimum(a,b):
    return b if a == maximum(a,b) else a

print(minimum(3,15))


#------------------------------ Exercício 08 --------------------------------#

"""

Escreva uma função (o contrato não pode ser esquecido) que elimine a casa das unidades de
um número inteiro. Por exemplo, retira(537) devolve 53. Se o argumento só tiver algarismo
das unidades, a função deve devolver 0. Teste a função escrevendo um programa que
receba um número inteiro do utilizador e imprima o resultado de chamada à função
desenvolvida.

"""
# My Option:
    
# def retira(a):
#     a = tuple(str(int(a)))
#     a = list(a)
#     a.pop()
#     a = ''.join(a)
#     if a == '':
#         a = 0
#     return(a)
    
# print(retira(537))

# # Second Option:

# def retirar(numero):
#     str_numero = (str(numero))
#     if len(str_numero) <2:
#         return 0
#     else:
#         return int(str_numero[:-1])
        
# print(retirar(537))

# # Third Option:

def retirei(numero):
    return numero // 10

print(retirei(537))

#------------------------------ Exercício 09 --------------------------------#

def multi(number):
    return print(number * 10 if number != 0 else 0)
    
multi(13)

#------------------------------ Exercício 10 --------------------------------#


def SomaDivisores(num):
    divisors = [1]
    for i in range(2, num):
        if (num % i)==0:
            # se o resto for igual a 0
            divisors.append(i)
    return sum(divisors)

while True:
    num = int(input("Escreva um número inteiro positivo:"))
    print('A soma dos divisores é: ', SomaDivisores(num))
    if num <= -1:
        print("O número deve ser positivo.")
        break
#------------------------------ Exercício 11 --------------------------------#


def is_prime(n):
    for i in range(3, n):
        if n % i == 0:
            return print("This isn't a prime number.")
    return print('The number is prime.')

is_prime(13)

#------------------------------ Exercício 12 --------------------------------#

n=int(input("Enter the number:: "))

for i in range(2,n):
    p=i
    k=0
    for j in range(2,p-1):
        if(p%j==0):
            k=k+1
    if(k==0):
        print(p)






