#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 30 09:28:37 2020

@author: filipeteto
"""
# import csv
# with open('eggs.csv', newline='') as csvfile:
#      spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
#      for row in spamreader:
#          print(', '.join(row))
 
"""
   
Comma Separated Values (CSV) é o formato mais comum de importação/exportação de
dados para folhas de cálculo e para bases de dados. A linguagem Python conta com um
módulo para o efeito, CSV File Reading and Writing. O padrão mais comum de utilização do
CSV para leitura é o seguinte:
"""
# import csv
# with open('ficheiro.csv', 'rU') as ficheiro_csv:
#     leitor = csv.reader(ficheiro_csv, delimiter=';')
# # No código acima leitor é um iterador. Pode ser usado num ciclo for, como por exemplo:
#     for linha in leitor:
# <fazer algo com linha>

# """
# Alternativamente, podemos chamar o método leitor.next() para obter os sucessivos
# elementos no iterador. A exceção StopIteration é levandada quando tentamos fazer um
# next() e não há mais elementos no iterador. A razão para usar 'rU' está relacionado com a
# função open(), e não propriamente com CSV. O parâmetro 'U' (de Universal), aceita linhas
# com a convenção Unix ('\n') e Windows ('\r\n'). O delimitador ';' é importante pois o formato
# CVS para a língua portuguesa utiliza o ponto-e-vírgula como separador, uma vez que a
# vírgula é utilizada como separador da parte decimal dos números. Escreva uma função
# csvLinhaParaGrafico que leia dum ficheiro CSV o gráfico de uma função e o devolva. A
# função recebe o nome de um ficheiro. Assuma que o ficheiro é composto por duas linhas, a
# primeira contendo os valores das abcissas, a segunda os valores das ordenadas. Não se
# esqueça de converter as strings lidas do ficheiro em números em vírgula flutuante.
#     """
import csv
def le_graficos(ficheiro_csv):
    grafico = []
    with open(ficheiro_csv) as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=";", quotechar='|')
        linha1 = csv_reader.__next__()
        linha2 = csv_reader.__next__()
        for i in range(len(linha1)):
            item_linha1_com_ponto = linha1[i].replace(',','.')
            item_linha2_com_ponto = linha2[i].replace(',','.')
            grafico.append((float(item_linha1_com_ponto), float(item_linha2_com_ponto)))
    return grafico
print(le_graficos("graficos.csv"))            
            
            
def converte_para_notacao_ponto(item1,item2):
    return float(item1.replace(',','.')),float(item2.replace(',','.'))
def le_graficos2(ficheiro_csv):
    grafico = []
    with open(ficheiro_csv) as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=";", quotechar='|')
        linha1 = csv_reader.__next__()
        linha2 = csv_reader.__next__()
        grafico = list(map(converte_para_notacao_ponto,linha1,linha2))
    return grafico
print(le_graficos2("graficos.csv"))               
            
            