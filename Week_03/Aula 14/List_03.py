#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 29 09:46:27 2020

@author: filipeteto
"""
#-------------------------------- Ficha 03 ----------------------------------#

#------------------------------ Exercício 01 --------------------------------#

# Qual o valor de cada expressão?
a = list(map(lambda x : x+1, range(1,4))) # [2,3,4]
# b = list(map(lambda x : x>0, [3,−5,−2,0])) # Devolve númerios maiores que 0
c = list(filter(lambda x : x>5, range(1,7))) # [6]
d = list(filter(lambda x:x%2==0, range(1,11))) # Even numbers from 1 to 10


#------------------------------ Exercício 02 --------------------------------#

# 2. Determine o valor de cada uma das expressões seguintes:
import functools
a = functools.reduce(lambda y, z: y* 3+z, range(1,5)) # 58
b = functools.reduce (lambda x, y: x** 2+y, range(2,6)) # 2814