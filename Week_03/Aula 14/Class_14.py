#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 29 10:33:27 2020

@author: filipeteto
"""

# f = open("blopfile.txt", "w") 
# "w" - Escreve iformação para o ficheiro apagando o que estava já escrito no mesmo
# Caso o ficheiro não exista, irá criar um.

# f = open("blopfile.txt", "a") 
# "a" - Escreve informação no final do ficheiro sem apagar o que já existia.

# f = open("blopfile.txt", "x")
# "x" - Cria um ficheiro e dá erro se o ficheiro já existir.

# f = open("blopfile.txt", "r")
# "r" - Abre-se um ficheiro para leitura. 

# Open - abre ligação até ao ficheiro.

# read_result = (f.read(5))

# print(read_result)
# read_result = f.readline()

# print(f.readline())
# print(f.readline())
# print(f.readline())

# for line in f:    # Forma semelhante de fazer readline para cada linha do ficheiro
#     print(line)


# with open ("blopfile.txt") as file_reader:   # utilizando o with não existe 
#     for line in file_reader:                 # necessidade de utilizar o f.close()
#         print(line)

# # f.close() # fecha ficheiro
# print(file_reader.closed) # Confirma se ficheiro fechou

# f = open("blopfile.txt", "a") 
# f.write("\n\nNow the file has more content!")
# f.close()

# f = open("blopfile.txt", "r")
# print(f.read())

# f = open("blopfile.txt", "w") 
# f.write("Woops! I have deleted the content!")
# f.close()

# f = open("blopfile.txt", "r")
# print(f.read())

# import os
# print(os.path.abspath("blopfile.txt")) # imprime o caminho absoluto
# print(os.path.relpath("blopfile.txt")) # imprime o caminho relativo
# print(os.path.basename("./blopfile.txt")) # dá o nome do ficheiro

# os.remove("blopfile.txt") # apaga o ficheiro

# with open ("blopfile.txt", "r") as file_reader:
#     lines = file_reader.readlines() #le as linhas para uma lista de strings
#     for line in lines:
#         line = line.replace('s','p')
#     print(lines)

# with open ("blopfile.txt", "w") as file_writer:
#     file_writer.writelines(lines)

# with open ("blopfile.txt") as file_reader:
#     lines_after_writing = file_reader.readlines()
#     print(lines_after_writing)
    
# try:    
#     a = int("12")
#     a / 0
# except ValueError:
#     print("The sting must contain numbers, not letters.")
# except:
#     print("Alguma coisa correu mal")
# finally:
#     print("thnx")
    
    
    
# f = open("blopfile.txt")
# try:
#     #do something with file content
#     except:
#     # handle exception
# finally:
#     f.close()



# with open ("blopfile.txt") as file_reader:
#     try:
#         file_reader.readlines()
#         # do something
#     except expression as identifier: # as  é para pegarmos na excepção e modifica-la no bloco except
#         identifier
#         pass
