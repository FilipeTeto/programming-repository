#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 30 12:00:06 2020

@author: filipeteto
"""

#---------------------------------- Ficha 08 --------------------------------#

"""
1. Crie uma classe que modele uma pessoa:
a. Atributos: nome, idade, peso e altura
b. Métodos: Envelhercer, engordar, emagrecer, crescer. Obs: Por padrão, a cada ano que nossa
pessoa envelhece, sendo a idade dela menor que 21 anos, ela deve crescer 0,5 cm.
2. Desenvolva uma classe Macaco,que possua os atributos nome e bucho (estomago) e pelo
menos os métodos comer(), verBucho() e digerir(). Faça um programa ou teste
interativamente, criando pelo menos dois macacos, alimentando-os com pelo menos 3
alimentos diferentes e verificando o conteúdo do estomago a cada refeição. Experimente
fazer com que um macaco coma o outro. É possível criar um macaco canibal?
"""


# 1 - 

class pessoa:
    
    
    def __init__(self, nome, idade, peso, altura):
        self.nome = nome
        self.idade = idade
        self.peso = peso
        self.altura = altura
        
    def  envelhecer(self,idade,altura):
        self.idade = idade
        self.altura = altura + 0.5 if idade < 21 else altura
        
# 2 -     

class macaco:

    def __init__(self, nome):
        self.nome = nome
        self.bucho = []

    def comer(self, objeto):
        self.bucho.append(objeto)

    def verBucho(self):
        print("Coisas no Bucho: ")
        for i in self.bucho:
            print(i)
        print("...")

    def digerir(self):
        print("Digerindo...")
        self.verBucho()
        self.bucho = []
        
        
        
king_kong = macaco("King-Kong")  
macaco1 = macaco("Adriano")
king_kong.comer("banana")  
king_kong.verBucho()
king_kong.digerir()
king_kong.comer(macaco1)
king_kong.verBucho()

        
        
        
        
        
        
        
        
        
        
        