#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 27 08:45:28 2020

@author: filipeteto
"""

#--------------------------------- Aula 12 ----------------------------------#
import copy
a = ['abc','def', 'ghi']
b=copy.deepcopy(a)

#------------------------------- Exercício 01 -------------------------------#

"""
Escreva em linguagem Python um programa que leia a temperatura em graus
Farenheit e que a converta para Celsius.
"""
F =float(input('Insert the temperature in Farenheit: '))
C = (F - 32)/1.8
print("It's ", f'{C:.2f}', 'celsius.')

#------------------------------- Exercício 02 -------------------------------#

"""
Escreva um programa que peça o ano de nascimento do utilizador
  e que retorne a idade do utilizador ao final do ano.
 
"""
from datetime import date
birth = int(input('Insert the year you were born:'))
today = date.today()
age= abs((birth - today.year))

print('You are ', age, 'years old.')

#------------------------------- Exercício 03 -------------------------------#

"""
Escreva um programa que leia um hora em horas/minutos/segundos e que
traduza para segundos.
"""
from datetime import datetime
dt = datetime.today()
seconds = dt.timestamp()
print(f'{seconds:.2f}')

#------------------------------- Exercício 04 -------------------------------#
 
  
def convert(seconds): 
    seconds = seconds % (24 * 3600) 
    hour = seconds // 3600
    seconds %= 3600
    minutes = seconds // 60
    seconds %= 60
      
    return "%d:%02d:%02d" % (hour, minutes, seconds) 
      
 
n = 93485
print(convert(n)) 


#------------------------------- Exercício 05 -------------------------------#
"""
se no início a variavel x tiver o valor de 1, qual o valor no final.
Resposta: 1.

qual teria de ser o valor de x para no final ser -1:
Resposta: 0.

Qual a parte do programa que nunca será corrida.
Resposta: o segundo if.
"""
x = 0

if x == 1:
    x = x+1
    if x == 1:
        x = x+1
    else:
        x = x-1
else:
    x = x-1


#------------------------------- Exercício 06 -------------------------------#

"""
Escreva em liguagem Python um programa que leia num ano (>0) e escreva o
século a que pertence
"""
ano = int(input('Insira um ano:'))
temp = ano // 100
rest = ano % 100
print('Século', temp if rest == 0 else temp+1)


#------------------------------- Exercício 07 -------------------------------#
"""
Escreva em linguagem Python um programa que leia um número inteiro positivo
(menor ou igual a 5) e escreva no ecrã a sua representação em numeração
romana. Segue-se um exemplo da interacção com o computador (em itálico os
dados introduzidos pelo utilizador).
"""

"""Solução 1
"""

numero_inteiro = int(input("escreva um numero"))

# casos_especiais = { 
#     1: 'I', 
#     4: 'IV', 
#     5: 'V', 
#     9: 'IX', 
#     10: 'X', 
#     40: 'XL',
#     50: 'L', 
#     90: 'XC', 
#     100: 'C', 
#     400: 'XD', 
#     500: 'D', 
#     900: 'CM', 
#     1000: 'M'
# }

# valores_casos_especiais_invertidos = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1]

# numero_romano = ""

# for valor in valores_casos_especiais_invertidos:
#     coeficiente = numero_inteiro // valor # quantas vezes valor dos caoss especiais está contido no número
    
#     if coeficiente != 0: # se caso especial estiver contido no número
#         for y in range(coeficiente):
#             numero_romano += casos_especiais[valor] # adicionar ao caso especial ao número o numero de vezes que foi obtido em coeficiente

#     numero_inteiro = numero_inteiro % valor # remove ao numero inteiro o valor do caso especial, o número de vezes que for possível atravês da operação de resto de divisão

# print(numero_romano)

"""Solução 2
"""

tabelaInteiroRomano =  [(1000,'M'), (900,'CM'), (500,'D'), (400,'CD'), (100,'C'), (90,'XC'), (50,'L'), (40,'XL'), (10,'X'), (9,'IX'), (5,'V'), (4,'IV'), (1,'I')]

resultado = ""
for numero, letra in tabelaInteiroRomano:
    quantos = numero_inteiro // numero
    resultado += letra * quantos
    numero_inteiro -= numero * quantos

print(resultado)


#------------------------------- Exercício 08 -------------------------------#

"""
Escreva em linguagem Python um programa que leia uma data (ano, mês e dia
em separado) e um número inteiro x, e devolva uma nova data x dias mais à
frente.
"""

ano = int(input("ano: "))
mes = int(input("mês: "))
dia = int(input("dia: "))
dias_add = int(input("que dia será daqui a este número de dias? "))

meses_dias = {1: 31, 2: 28, 3: 31, 4: 30, 5: 31, 6: 30, 7: 31, 8: 31, 9: 30, 10: 31, 11: 30, 12:31}

e_ano_bisexto = (mes == 2 and ano % 4 == 0 and not (ano % 400 == 0 and ano % 100 == 0))
dias_do_mes = meses_dias[mes] + e_ano_bisexto # you can add boolean expressions to numbers, as if True it will be interpreted as one and if False it will be interpreted as 0

while dia + dias_add > dias_do_mes:
    ano += (mes + 1) > 12 # é adicionado um se o proximo mês for menor que 12 (Dezembro)
    mes = (mes + 1) % 12 if mes + 1 > 12 else mes + 1 # se o mês for maior que doze incrementamos o mês o obtemos o resto da divisão (ex: mês = 13 então mês % 12 = 1)
    dias_add -= dias_do_mes - dia # removemos os restantes dias que faltam no mês dos dias adicionados
    dia = 0 # pomos o dia como 0 para adicionar-mos os restantes dias adicionais caso saia do loop ou no próximo loop sem ter um dia a mais
    dias_do_mes = meses_dias[mes] + e_ano_bisexto # atualizamos os dias do mês

dia += dias_add # adicionamos os restantes dias do mês

print(f"{ano}-{mes}-{dia}") 

#------------------------------- Exercício 09 -------------------------------#

"""
9. Indique os erros sintáticos no seguinte programa em Python:
    
"""


x = 1
y = 1
while y < 5:
    if x == 1:
        y += 2
        print(x)
        print(y)
#------------------------------- Exercício 10 -------------------------------#

















