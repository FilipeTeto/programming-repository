#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 28 08:39:42 2020

@author: filipeteto
"""

#---------------------------------- Aula 13 ---------------------------------#
"""
# Functions 
     . always returns
     . def <function name>(<mandatory parameter>, <optional parameter> = <default value>)
       <function name>(<mandatory parameter>)
     . high level functions - functions that receive other functions as parameters
     . functions can be called inside fuctions to avoid a extense code
     
       
# Procedures 
     . modifies the state of the program without returning anything

# Generators 
     . return different values as asked

"""
# Function - Example 1

# def multinum(num,value = 2):
#     return num*value

# print(multinum(2))


# Function - Example 2

# a = [['a',2], ['c',13], ['b',1]]
# a.sort()
# print(a)


# Function - Example 3

# a = [['a',2], ['adzdv',13], ['b',1], ['awdsrh', 999]]
# def compare_items(item):
#     return len(item[0])

# a.sort(key=compare_items)
# print(a)


# Procedure - Example 1

# a = 'c'
# def triplica(a):
#     a= a*3

# print(triplica(a))

# Generator - Example 1

# a = range(0,10,2)

# def range(start,end, step = 1):
#     i = start
#     while i < end:
#         yield i
#         i += step

# for i in a:
#     print(i)

"""

yield :  para a execução e guarda estado da função.

"""

"""

3 Funções Essenciais para saber em Python:
    
    Map - aplica um função a cada elemento da lista. 
 
         Substitui: def aplica_funcao_a_elementos(lista,funcao):
                        for i in range(len(lista)):
                            lista[i] = funcao(lista[i])
                        return lista
                    def add_two (x):
                        return x+2
                    
                    aplica_funcao_a_elementos([2,5,1,8],add_two)
                    
                
              Por: map(funcao, iterables)
    Filter -
    
    Sorted - 
"""

#--------------------------------- Map --------------------------------------#

# def aplica_funcao_a_elementos(lista,funcao):
#     for i in range(len(lista)):
#         lista[i] = funcao(lista[i])
#     return lista
# def add_two (x):
#     return x+2
                    
# aplica_funcao_a_elementos([2,5,1,8],add_two)

# lista = [2,5,1,8]
# a = list(map(add_two,lista))
# print(a)


# def multiply(a,b):
#     return a*b

# a = map(multiply, (1,2,3),(2,3,4))     
# a = list(a)               


#-------------------------------- Filter ------------------------------------#

# a = [1,22,33,2,16]

# def lt_dezasseis(x):
#     return x < 16

# filtered_a = list(filter(lt_dezasseis,a))
# print(filtered_a)


#-------------------------------- Sorted ------------------------------------#

# a = ["abc", "asdafsrgd", "jthrgefw"]
# sorted_a = sorted(a,key=len)
# print(sorted_a)

#-------------------------------- Reduce ------------------------------------#
"""

result = ''
for item in lista:
    result = funcao(item)
    
"""
# a = 'abcdefghi'
# def my_sum(lista):
#     accumulator = type(lista[0])()
#     for item in lista:
#         accumulator += item
#     return accumulator

# sumed_a = my_sum(a)
# lista_listas= [[1,2],[3,4],[5,6]]
# sumed_lista_listas =my_sum(lista_listas)

# def add(a,b):
#     return a+b

# from functools import reduce
# sumed_lista_listas = reduce(add, lista_listas)



# fruits = ["banana","apple","peach"]
# sumed_fruits = reduce(add, fruits, "I love")
# print(sumed_fruits)

# sumed_fruits = reduce(lambda x,y: x + y, fruits)
# print(sumed_fruits)


"""

Lambda : indica que estamos a definir uma função sem nome ou temporária
    
"""    

#----------------------------- Compressao de Listas -------------------------#

# [x+2 for x in lista_x]
# result = []

""" Ao invés de: """

# for item in lista x:
# result.append(item)


a = [[1,2],[3,4]]
flattened_a = [sub_item for item in a for sub_item in item] 
print(flattened_a)

#   ou 

from functools import reduce
flattened_a = reduce(lambda x,y: x+y, a)
print(flattened_a)

"""
ao invés de:

for item in a:
    for sub_item in item:
        print(item)
        
for item in a:
    for sub_item in item:
        print(sub_item)

"""

