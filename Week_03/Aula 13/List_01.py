#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 28 11:22:23 2020

@author: filipeteto
"""

#------------------------------- Exercício 01 -------------------------------#

f = float(input('Please enter temperature: '))
c = (f - 32) // 1.8
print ('The temperature in Celcius is: ',c)


#------------------------------- Exercício 02 -------------------------------#

birth_year = int(input('Please enter your year of birth:'))
print ('You are', 2020 - birth_year, 'years old.')


#------------------------------- Exercício 03 -------------------------------#

h = '01:28:42'
h_comp = h.split(':')
secs_total = int(h_comp[0]) * 3600 + int(h_comp[1]) * 60 + int(h_comp [2])
print (secs_total)


#------------------------------- Exercício 04 -------------------------------#

# hora = 3600 seg
# minuto = 60 seg
ttl_seg = 9999
ttl_hour = ttl_seg // 3600
ttl_min = (ttl_seg-(ttl_hour*3600)) // 60
ttl_segu = ttl_seg - (ttl_hour*3600 + ttl_min*60)

print (ttl_hour, ':', ttl_min, ':', ttl_segu)


#------------------------------- Exercício 05 -------------------------------#


x = 1
if x == 1:
    x = x + 1   # x = 2
    if x == 1:
        x = x + 1
    else:
        x = x - 1
else:
    x = x - 1   # x = 1
    

#------------------------------- Alínea A)

"""
O valor final é de 1, porque o else que está dentro do primeiro if 
retira 1 unidade ao valor de x. 

"""


#------------------------------- Alínea B)

"""
O valor teria de ser 0.

"""


#------------------------------- Alínea C)

"""
O segundo if nunca é executado porque, como tem a mesma condição
do if anterior e no if anterior existe uma transformação de x 
dá skip para o else.
"""


#------------------------------- Exercício 06 -------------------------------#

roman_num = { 1:'I', 2:'II', 3:'III', 4:'IV', 5:'V'}
num = int(input("Please, pick a number from 1 to 5: "))

print("This is your roman number: ", roman_num.get(num)) 


#------------------------------- Exercício 07 -------------------------------#

"""
Escreva em linguagem Python um programa que leia um ano (> 0) e escreva no ecrã o século
a que este ano pertence. Relembre que o ano 1999 faz parte do século XX, 2000 ainda faz
parte do século XX e que 2001 é o primeiro ano do século XXI. Desenvolva uma versão do
programa utilizando um comando alternativo e outra utilizando apenas uma expressão
numérica. Note que int(True)=1 e que int(False)=0.

"""

ano = int(input("Type a year: "))
temp = ano // 100
resto = ano % 100
print (temp if resto == 0 else temp +1)


#------------------------------- Exercício 08 -------------------------------#

"""
Escreva em linguagem Python um programa que leia uma data (ano, mês e dia em separado)
e um número inteiro x, e devolva uma nova data x dias mais à frente.

"""

ano = int(input("Ano: "))
mes = int(input("Mês: "))
dia = int(input("Dia: "))
dias_add = int(input("Indique um número de dias para adiar: "))

meses_dias = {1: 31, 2: 28, 3: 31, 4: 30, 5: 31, 6: 30, 7: 31, 8: 31, 9: 30, 10: 31, 11: 30, 12:31}

e_ano_bisexto = (mes == 2 and ano % 4 == 0 and not (ano % 400 == 0 and ano % 100 == 0))
dias_do_mes = meses_dias[mes] + e_ano_bisexto 
# you can add boolean expressions to numbers, as if True it will be interpreted as one and if False it will be interpreted as 0

while dia + dias_add > dias_do_mes:
    ano += (mes + 1) > 12 
    # é adicionado um se o proximo mês for menor que 12 (Dezembro)
    mes = (mes + 1) % 12 if mes + 1 > 12 else mes + 1 
    # se o mês for maior que doze incrementamos o mês o obtemos o resto da divisão (ex: mês = 13 então mês % 12 = 1)
    dias_add -= dias_do_mes - dia 
    # removemos os restantes dias que faltam no mês dos dias adicionados
    dia = 0 
    # pomos o dia como 0 para adicionar-mos os restantes dias adicionais caso saia do loop ou no próximo loop sem ter um dia a mais
    dias_do_mes = meses_dias[mes] + e_ano_bisexto 
    # atualizamos os dias do mês

dia += dias_add 
# adicionamos os restantes dias do mês

print(f"{ano}-{mes}-{dia}")


#------------------------------- Exercício 09 -------------------------------#

x = 1
y = 4
if x==1 and y<5:
    y=y+2
print (y)


#------------------------------- Exercício 10 -------------------------------#

"""
Este programa em Python tem como objectivo escrever a tabuada do número inteiro dado
pelo utilizador. Explique porque é que este programa não termina, corrija o erro e
especifique qual o valor das variáveis n e i no final da execução do programa corrigido:
"""

# n = int(input("Escreve um número inteiro: "))
# print("Tabuada do", n, ":")
# i = 1
# while i <= 10:
#     print(n, "x", i, "=", n * i)
#     i + 1

"""
O programa não termina porque i+1 deve ser i+=1 (i = i+1)

"""

n = int(input("Escreve um número inteiro: "))
print("Tabuada do", n, ":")
i = 1
while i <= 10:
    print(n, "x", i, "=", n * i)
    i+=1


#------------------------------- Exercício 11 -------------------------------#

"""
Considere este programa em Python:
"""

dividendo = int(input("Dividendo: "))
divisor = int(input("Divisor: "))
resto = dividendo
quociente = 0
while resto >= divisor:
    resto = resto - divisor
    quociente = quociente + 1
    print("O quociente é", quociente, "e o resto é", resto)


#-------------------------------Alínea A)

"""
O programa demonstra o número de vezes que é possível aplicar o divisor ao
dividendo mostrando sempre o resto.
O programa irá correr até não ser possível dividir mais o dividendo pelo divisor
indicado.

"""


#-------------------------------Alínea B)

"""
Quais são as duas operações aritméticas disponíveis em Python que implementam a
mesma funcionalidade?


O % e //.
"""


#-------------------------------Alínea C)

while True:
    dividendo = int(input("Dividendo: "))
    divisor = int(input("Divisor: "))
    if dividendo<divisor:
        print("O dividendo tem de ser maior do que o divisor.")
        continue
    else:
        resto = dividendo
        quociente = 0
        while resto >= divisor:
            resto = resto - divisor
            quociente = quociente + 1 
            print("O quociente é", quociente, "e o resto é", resto)
        break        
    

#-------------------------------Alínea D)
   
while True:      
    dividendo = int(input("Dividendo: "))
    divisor = int(input("Divisor: "))
    if dividendo<=0 or divisor<=0:
        print("O dividendo e o divisor têm de ser números positivos.")
        continue
    elif dividendo<divisor:
        print("O dividendo tem de ser maior do que o divisor.")
        continue
    else:
        resto = dividendo
        quociente = 0
        while resto >= divisor:
            resto = resto - divisor
            quociente = quociente + 1 
            print("O quociente é", quociente, "e o resto é", resto)
        break    


#------------------------------- Exercício 12 -------------------------------#
"""
O seguinte programa em Python escreve no ecrã os factoriais de todos os números
inteiros entre 1 e n, em que n é dado pelo utilizador:
"""    
n = int(input("Escreve um número inteiro: "))
current_n = 1
while current_n <= n and current_n <1000:
    i = current_n
    f = 1
    while i > 1:
        f = f * i
        i = i - 1
        print("Factorial de " + str(current_n) + ": " + str(f))
        current_n = current_n + 1

        
#------------------------------- Exercício 13 -------------------------------#

"""
Escreva um programa em Python que peça ao utilizador um número decimal e escreva no
ecrã a sua parte inteira perguntando em seguida se o utilizador quer terminar a utilização do
programa.
"""
def float_to_int():
    user_float = float(input("Insert a float number: "))
    return print("The whole number is: ", (int(user_float // 1)))
float_to_int()   

while True:
    answer = input("Do you want to exit the program? Y/N ")
    if answer == 'N':
        float_to_int()
        continue
    elif answer == 'Y':
        break
    else:
        print("That's not a valid answer.")
        continue
    
    
#------------------------------- Exercício 14 -------------------------------#

k = int(input("Insira um número inteiro: "))
w = str(input("Insira uma palavra: " ))

for i in range(1, k + 1):
    print(i, (w + " ") * i)


#------------------------------- Exercício 15 -------------------------------#

powpow = int(input("Insert the power number: "))
powpowtotal = 1
for i in range(0,powpow+1):
   powpowtotal = powpowtotal*3**i
print("The value is: ", powpowtotal)


#------------------------------- Exercício 16 -------------------------------#

"""
Escreva um programa em Python que que peça ao utilizador um número inteiro k maior do
que 2 e escreva no ecrã quantos números perfeitos existem entre 2 e k (inclusive). Por
exemplo, existem 4 números perfeitos entre 2 e 10000 (o 6, o 28, o 496 e o 8128).
"""

def divisores(x): 
    return [y for y in range(1, int(x / 2) + 1) if x % y == 0] 
 
def numeros_perfeitos_in_range(a,b): 
    return [x for x in range(a,b+1) if sum(divisores(x)) == x] 
 
print(numeros_perfeitos_in_range(1, 10000)) 
        

#------------------------------- Exercício 17 -------------------------------#

"""
Escreva um programa em Python que peça ao utilizador um número inteiro k maior do que
10 e escreva no ecrã quantas capícuas existem entre 10 e k. Uma capícua é um número que
se lê de igual forma da esquerda para a direita e da direita para a esquerda. Por exemplo,
entre 10 e 100 existem 9 capícuas.
"""
try:
    num_c=0
    k=int(input("Insira um número inteiro maior do que 10:" ))
    if k<10:
        print("Insira um número válido.")
    else:
        for c in range(10,k+1):
            c=str(c)
            if c==c[::-1]:
                num_c+=1
            else:
                continue
    print(num_c)
except:
    print("Insira um número válido.")
            
    
