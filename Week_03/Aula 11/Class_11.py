#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 26 08:50:39 2020

@author: filipeteto
"""

#-------------------------------- Aula 11 -----------------------------------#



#------------------------------- Exercise -----------------------------------#


# """
# Find number of x occurrences in S
# """
# #input:

# s = 'Python Course'

# x = ['o', 'r']

# d = {}


# # Option 1

# #Output : {'o':2, 'r':1}

# for i in s:
#     if i in x:
#         d[i] = d.get(i, 0) + 1    
# print(d)


# # Option 2

# for i in s:
#     if i in x:
#         d.setdefault(i, 0)
#         d[i] += 1
# print(d)

# #------------------------------- Exercise -----------------------------------#

# """
# Remove duplications 
# find the bug in this example
# """

# d = {'x':3, 'y':2, 'z':1, 'y':4, 'z':2}
# r={}

# for k,v in d.items():
#     if k not in r.keys():
#         r[k] = v
# print(r)

# # Dictionaries doesnt acept duplicate values


# #------------------------------- Exercise -----------------------------------#

# # List of dictionaries

# student = [{'id':123, 'name': 'Sophia', 's' : True},
#             {'id':456, 'name': 'William', 's': False},
#             {'id':789, 'name': 'Sara', 's': True}]
# s = 0
# # Count the output of 's == True' in students list . Output is 2
# """
# print(True+True)
# print(True+False)
# print(True+1)
# print(True+3.14)
# """


# for i in student:
#     s += (i['s'])
# print(s)


# #----------------------- Randomly choose between dict keys ------------------#

# d = {
#      'F': 0,
#      'B': 0}

# import random
# for _ in range(17):
#     d[random.choice(list(d.keys()))] += 1
# print(d)



#---------------------------------- Sets ------------------------------------#

# f = {'a','b', 'c'}
# f.add('d')
# # f.add('e','f') # Error, only 1 argument can be given
# # f.add(['e2','f2']) # Error, cant insert lists only tuples

# f.update('g')
# f.update('h', 'i')
# f.update(['h2','i2']) # Lists can be used in update, not on add.

# print(f)


#---------------------------------- Sets ------------------------------------#
# # Union, Intersection and Update

# x = {1, 2, 3}
# y = {2, 3, 4}

# print(x.union(y))
# print(x | y)

# print(x.intersection(y))
# print(x & y)

# #---------------------------------- Sets ------------------------------------#
# #Difference, difference_update and symmetric_difference

# A = {1,2,3,4,5}
# B = {2,4,7}

# print(A-B)
# print(B-A)

# r = A.difference(B)

# print(r)
# print(A)
# print(B)


# X = {1, 2, 3}
# Y = {2, 3, 4}

# print(X.symmetric_difference(Y))
# print(X^Y)
# print(X.union(Y) - X.intersection(Y))
# print(X.union(Y) - Y.intersection(X))

# r = A.difference_update(B)
# print(r)
# print(A)
# print(B)

#-------------------------------- Exercise ----------------------------------#

# A = {10,2,6,1,3,9,4}
# B = {13,14,15,4,1,9,3}
# C = {11,12,14,15,1,3,9,2,6}

# print(C.difference(A & B & C))
# print(A.intersection(B).difference(C))
# print(A.union(B).difference(C))


#---------------------------------- Sets ------------------------------------#
# Isdisjoint

# x = {1, 2, 3}
# y = {1, 2, 3}
# print(x.isdisjoint(y))

# x = {1, 2, 3}
# y = {3, 7, 8}

# print(x.isdisjoint(y))

#---------------------------------- Sets ------------------------------------#
# isSubset

# A = {1, 2, 4}
# B = {1, 2, 3, 4, 5}
# print(A.issubset(B))
# print(B.issubset(A))

#-------------------------------- Exercise -----------------------------------#

# A = {'a', 'y', 'c', 'o', 'z'}
# B = 'Python Course'
# B_set= set(B)
# print(A.intersection(B_set))

#-------------------------------- Exercise -----------------------------------#

# d1 = {'a':1, 'b':3, 'c':2}
# d2 = {'a':2, 'b':3, 'c': 1}

# set1 = set(d1.items())
# set2 = set(d2.items())

# set1.intersection_update(set2)

# d3 = dict(set1)

# print(d3)

#-------------------------------- Exercise ----------------------------------#
# Intervals, mathmatics
# output [a,d]
# a <= X < d


a = int(input('First number please:'))
b = int(input('Second number please:'))
c = int(input('Third number please:'))
d = int(input('Last number please:'))

a_b = set(range(a, b+1)) 
c_d = set(range(c, d))

if a > b:
    a,b = b,a
elif c > d:
    c,d = d,c
else:
    pass

if a in (a_b & c_d) and b not in (a_b & c_d):
    print('The intersection is between a and d.')
elif a not in (a_b & c_d) and b in (a_b & c_d):
    print('The intersection is between c and b.')
elif a_b == (a_b & c_d):
    print('This interval a_b is a part of c_d.')
elif c_d == (a_b & c_d):
    print('This interval c_d is a part of a_b.')
else:
    print('There is no intersection between intervals.')

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    












