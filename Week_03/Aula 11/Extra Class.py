#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 26 16:02:41 2020

@author: filipeteto
"""

#--------------------------------- Extra Class ------------------------------#

for title in range(3,20,4):
    print('First loop:', title)
    
for title in [3, 7, 11, 15, 19]:
    if title in [7, 15]:
        title += 2
        print('Second loop:', title)
        
    