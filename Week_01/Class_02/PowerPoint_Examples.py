#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 13 13:49:28 2020

@author: filipeteto
"""
#-------------------------------- Exemplo 1 ---------------------------------#

minimum_age =18
age =int(input("How old are you?"))
if age > minimum_age:
    print("Congrats, you can get your license")
else:
    print("Please, come back in {0} years".format(minimum_age - age)) 

# int para que o input seja comparado com age para que haja um dos resultados



#--------------------------------- Exemplo 2 --------------------------------#

name = input("What's your name?")
password = input("What's your password?")
if name == 'Filipe':
    print("Hello Filipe")
if password == 'programar':
    print('Welcome.')
else:
    print('Wrong user or password.')
   
# Caso o name ou password fosse diferente do estipulado em if, seria printado else.



#--------------------------------- Exemplo 3 --------------------------------#

