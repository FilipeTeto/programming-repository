#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 13 14:34:19 2020

@author: filipeteto
"""


# Exercises:
    
# 1 - Answer what type of object(variable) should be used to store each of the following
# information:
# a.	A person's age. - int
# b. 	The area of your yard in square meters. - float
# c. 	The amount of money in your bank account - float  
# d.	The name of your favorite songs. - str

# 2 - Write a Python program that accepts a word from the user and reverse it

word = input("Input a word to reverse: ")

for char in range(len(word) - 1, -1, -1):
  print(word[char], end="")
print("\n")




# 3 - Initialize the string “abc” on a variable named “s”:
    
s="abc"

# A - Use a function to get the length of the string.

print(len(s))

# B - Write the necessary sequence of operations to transform the string “abc” in “aaabbbccc”
#  Suggestion: Use string concatenation and string indexes.

print((s[0:1]*3)+(s[1:2]*3)+(s[2:3]*3))


# 4 - Initialize the string “aaabbbccc” on a variable named “s”:
    
s="aaabbbccc"

# # A - Use a function that allows you to find the first occurrence of “b” in the string, and the first occurrence of “ccc”.

res=s.find('b')

print("The first letter b in the string is:" + str(res))

# # B - Use a function that allows you to replace all occurrences of “a” to “X”, and then use the same function to change only the first occurrence of “a” to “X”.

print(s.replace('a', 'X', 3))

print(s.replace('a', 'X', 1))


# 5 - Starting from the string “aaa bbb ccc”, what sequences of operations do you need to arrive at the following strings? 
# You can use the “replace” function.

string="aaa bbb ccc"

# 1. “AAA BBB CCC”

print(string.upper())

# 2. “AAA bbb CCC

print(string.replace('b', 'B', 3))


# 6 - Consider the code snippet below:

a=10
b=a
c=9
d=c
c=c+1

print(a)
print(b)
print(c)
print(d)

# After executing this code snippet, what will be the value stored in each variable?

# A = 10
# B = 10
# C = 10
# D = 9

# 7 - Write a code that reads two integer values in the variables x and y and change the content of the variables. For example, assuming that x = 2 and y = 10 were the values read, your program should make x = 10 and y = 2. Redo this problem using only x and y
# as variables.

x=2
y=10

(x, y) = (10, 2)
print(x)
print(y)


# 8 - Consider a program that should classify a number as odd or even and, in addition,
# classify it as less than 100 or greater than or equal to 100. The solution below does this
# classification correctly?

# Comment: No. It needs better conditionals.
user_number=int(input('Type your number here:'))

if user_number > 100:
    
    if user_number %2:
        
        print('This number is odd and bigger than 100.')
        
    else:
        
        print('This number is even and bigger than 100.')

elif user_number == 100:
    
    if user_number %2:
        
        print('This option will never be printed.')
  
    else:
        
        print('This number equals 100 and its even.')


else: 
    
    if user_number %2:
        
        print('This number is odd and smaller than 100.')
    
    else:
        
        print('This number is even and smaller than 100.')
       
        
            

