# -*- coding: utf-8 -*-
"""
Created on Tue Oct 13 10:50:06 2020

@author: admin
"""

# print((3 / 2))      # Divisão de 3 por 2
# print((3 // 2))     # Faz a divisão de 3 por 2 e arredonda para baixo o resultado
# print((3 % 2))      # Remainder de 3 por 2
# print((3 * 2))      # Multiplica 3 por 2

# import statistics

# numseq1 = (2, 4)

# average = statistics.mean(numseq1)

# print(round(average, 2))


# numseq2 = (4, 8, 9)

# average = statistics.mean(numseq2)

# print(round(average, 2))


# numseq3 = (12, 15, 14/6)

# average = statistics.mean(numseq3)

# print(round(average, 2))

import math

def volume_sphere(radius_1,const,power):
    volume= const*math.pi* radius_1**power
    print('The volume of the sphere is: ', f"{volume:.3f}")
    return volume
radius_1=float(input("Please, enter sphere radius:"))
const=4/3
power=3    
volume_sphere(radius_1,const,power)

# Volume formated to 3 decimals
## Imported math to use pi
### Testing git -a update

# num = int(1)  
# if (num % 2) == 0:  
#    print("{0} is Even number".format(num))  
# else:  
#    print("{0} is Odd number".format(num))
   
# num = int(5)  
# if (num % 2) == 0:  
#    print("{0} is Even number".format(num))  
# else:  
#    print("{0} is Odd number".format(num))

# num = int(20)  
# if (num % 2) == 0:  
#    print("{0} is Even number".format(num))  
# else:  
#    print("{0} is Odd number".format(num))
   
# num = int(60/7)  
# if (num % 2) == 0:  
#    print("{0} is Even number".format(num))  
# else:  
#    print("{0} is Odd number".format(num))