#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 16 08:48:56 2020

@author: filipeteto
"""


#---------------------------------- Aula 5 ----------------------------------#


#-------------------------------- Exemplo 01 --------------------------------#


# def food(vegetable):
#     if vegetable == 'tomato':
#         print('I bought a tomato.')
#     elif vegetable == 'orange':
#         print('I bought an orange.')
#     else:
#         print('I bought other vegetable')

# food('banana')


#------------------------------- Exemplo 02 ---------------------------------#


# def addtwo (a,b):
#     added = a + b
#     subbed = a - b
#     return added, subbed

# x= addtwo (3, 5)
# print(x[0])
# print(x[1])


#------------------------------- Exemplo 03 ---------------------------------#


# def food():
#     eggs = 'food local'
#     print(eggs) # prints 'food local'

# def more_food():
#     eggs = 'more_food local'
#     print(eggs) # prints 'more_food local'
#     food()
#     print(eggs) # prints 'more_food local'
    
# eggs= 'global'
# more_food()
# print(eggs) # prints 'global'


#------------------------------- Exemplo 04 ---------------------------------#


a=3
b=10
c=0

c= a + b
c += a # c + a
c *= a # c * a
c /= a # c / a float
c = 4
c%= a # c % a
c**= a # c**a
c//= a # c / a int


#------------------------------- Exemplo 05 ---------------------------------#


# a= 10
# b= 3

# hifen=30
# binary= bin(a & b);
# denary= a & b
# print("line 1 - Value of binary is", binary)
# print("line 1 - Value of denary is", denary)
# print("-"*hifen)


#------------------------------ Exercise 01 ---------------------------------#

def even_odd(num):

    for i in range(num):
        if i % 2 == 0:
            print('This', num ,"is even.")
        else:
            print('This', num,'is odd.')
        num -= 1
        
even_odd(7)
           
#------------------------------ Exercise 02 ---------------------------------#


def mean(lst):
    
    return sum(lst) / len(lst)

lst = [2, 61, -1, 0, 88, 55, 3, 121, 25, 75]

print('The mean of list is:', mean(lst))
