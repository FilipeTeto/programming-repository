#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 15:02:35 2020

@author: filipeteto
"""
#-------------------------------- Lista 02 ----------------------------------#


#------------------------------ Exercício 01 --------------------------------#


def add(x, y):
    return x + y


def subtract(x, y):
    return x - y


def multiply(x, y):
    return x * y


def divide(x, y):
    return x / y


print("Select operation.")
print("A.Add")
print("B.Subtract")
print("C.Multiply")
print("D.Divide")

while True:
    # Take input from the user
    choice = input("Enter choice(A/B/C/D): ")

    # Check if choice is one of the four options
    if choice in ('A', 'B', 'C', 'D'):
        num1 = float(input("Enter first number: "))
        num2 = float(input("Enter second number: "))

        if choice == 'A':
            print(num1, "+", num2, "=", add(num1, num2))

        elif choice == 'B':
            print(num1, "-", num2, "=", subtract(num1, num2))

        elif choice == 'C':
            print(num1, "*", num2, "=", multiply(num1, num2))

        elif choice == 'D':
            print(num1, "/", num2, "=", divide(num1, num2))
        break
    else:
        print("Invalid Input")
        
    
#---------------------------- Exercício 02 ----------------------------------#


print('Today we are serving:')
print('\n1. Pão Alentejano')
print('2. Bolo Lêvedo dos Açores')
print('3. Bolo do Caco da Madeira')
print('4. Broa')
print('5. I want to leave')

while True:
    line = input('Type here:')
    if line == 'I want to leave':
        break
    print("Try again:")   
print("Be gone demon.")


#------------------------------ Exercício 03 --------------------------------#


value1 =ord('F')

x= 5 + value1
y= 0

while True:
    y=(x%2)+ 10*y
    x= x//2
    print('x=', x,'y=', y)
    if x==0:
        break

while y!=0:
    x=y%100
    y=y // 10
    print('x=', x, 'y=', y)    


#------------------------------ Exercício 04 --------------------------------#


n=1

while n <6:
    print (str(n))
    n = n + 1
  


