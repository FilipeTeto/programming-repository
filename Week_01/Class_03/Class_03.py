#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 09:28:21 2020

@author: filipeteto
"""


#---------------------------------- Aula 3 ----------------------------------#


#---------------------------- Tabela de Booleans ----------------------------#


print(4==5)
print(6>7)
print(15<100)
print('hello' == 'hello')
print('hello' == 'Hello')
print('dog' != 'cat')
print(  True == True)
print(True != False)
print(  42 == 42.0)
print( 42 == '42')
print('apple' == 'Apple')
print('apple' > 'Apple')
print('A unicode is', ord ('A'),', a unicode is', ord('a'))


#-------------------------------- Exemplo 1 ---------------------------------#


minimum_age =18
age =int(input("How old are you?"))
if age > minimum_age:
    print("Congrats, you can get your license")
else:
    print("Please, come back in {0} years".format(minimum_age - age)) 

#int para que o input seja comparado com age para que haja um dos resultados



#--------------------------------- Exemplo 2 --------------------------------#


name = input("What's your name?")
password = input("What's your password?")
if name == 'Filipe':
    print("Hello Filipe")
    if password == 'programar':
        print('Welcome.')
    else:
        print('Wrong password')
    
else:
    print('Wrong user.')
   
# Caso o name ou password fosse diferente do estipulado em if, seria printado else.


#---------------------------------- Exemplo 3 -------------------------------#


mininum_age= 18
user_age=int(input("How old are you?"))
if user_age < mininum_age:
    print("Please come back in {0} years".format(mininum_age - user_age))
elif user_age > 75:
    print("Probably, you cannot drive anymore. Please, make an appointment with your doctor.")
else:
    print("Congrats, you can get your license.")


#---------------------------------- Exercício 1 -----------------------------#


guess_number=int(13)
user_number=int(input("Guess if you're number is bigger or smaller than mine. Insert your number here: "))
if user_number < guess_number:
    print("Guess higher!")
elif user_number > guess_number:
    print("Guess lower!")
else:
    print("Bullseye mate!")


#---------------------------------- Exercício 2 -----------------------------#


age_given=int(input("Please, enter your age here:"))

print("You are " + str(age_given) + " years old.")

if (16 <= age_given <= 65):
    print("Have a good day at work.")
elif (age_given <16):
    print("You're too young to work")
else:
    print("You have worked enough! Let's travel now!")


#--------------------------- Comparisons using print ------------------------#


print((4<5) and (5<6))
print((4<5) and (9<6))
print((1==2) or (2==2))
print((2+2==4) and not (2+2==5) and (2*2==2+2))


#---------------------------- Code using conditionals -----------------------#


a=3
b=2
if a==5 and b>0:
    print("a is 5 and", b, "is greater than zero.")
else:
    print('a is not 5 or', b, 'is not greater than zero.')


#---------------------- Be careful using conditionals -----------------------#

goodweather = True
day=str(input("What day is it today?"))
temperature=int(input("How's the wheather in celcius?"))
raining=input("Is it raining?")

if day == 'Saturday':
    if temperature >= 20:
        if raining == 'no':
            print("Go out and enjoy the sun.")
        else:
            print("Better finish my Python programming exercises!")
    else:
         print("Better finish my Python programming exercises!")
else:
     
    print("Better finish my Python programming exercises!")

    
    

#----------------------------- Repeated Steps -------------------------------#


n=5
while n > 0:
    print(n)
    n = n-1
print(n)
print("Boom!")


#----------------------------- Infinite Loop --------------------------------#

while True:
    line = input('>')
    if line[0] == "#":
        continue
        print(line)
    elif line == 'enough':
        break
        print(line)
print("Done!")

 #-------------------------------- Slide 20 ---------------------------------#
 
found = False
print ('Before', found)
for value in [9, 41, 12, 3, 74, 15] : 
   if value == 3:
         found = True      
         print(found, value)
   else:
         found = False
         print (found, value)
   
print ('After', found)













