#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 12 15:18:47 2020

@author: filipeteto
"""


#----------------------------- Exercício 5 ----------------------------------#


quantity = 3

totalMoney = 1000

price = 450

statement1 = "I have {1} dollars so I can buy {0} football for {2:.2f} dollars."
print(statement1.format(quantity,totalMoney, price))