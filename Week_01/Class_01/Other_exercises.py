# -*- coding: utf-8 -*-
"""
Spyder Editor



author: filipeteto
"""


#-----------------------------------------------------------------------------

frase = "Lisbon is in Portugal"

# print(frase)

print(frase[0])
print(frase[1])
print(frase[2])
print(frase[3])
print(frase[14])
print(frase[19])

print(frase[0:6])

print(frase[0:9])



# Deixar em comentário o que o código faz
    # Permite relembrar o que foi feito

a = 12
b = 3

print(a + b)    #15
print(a - b)    #9
print(a * b)    #12
print(a / b)    #4.0
print(a // b)   #4 integer division arredonda para baixo
print(a % b)    # 0 modulo: the 

print(type(a)) # permite identificar o tipo de dado (str,int,float, etc)

import sys

print("\nInteger value information: ",sys.int_info)

print("\nMaximum size of an integer: ",sys.maxsize)