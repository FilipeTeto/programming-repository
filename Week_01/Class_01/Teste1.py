# -*- coding: utf-8 -*-
"""
Created on Mon Oct 12 10:48:34 2020

@author: admin
"""

# print(2)

# print("Welcome to python course")

# print('Welcome to python course')

# print(8+5*13)

# print("Python is fun", 31)

# print("Hello" + "Filipe")

# print("Hello"' '"Filipe")

# print("Hello" + "Filipe")

# print("Hello, Filipe")

# print("Hello " + "Filipe")

# print("Hello"+" "+"Filipe")
#
# print("Hello"," ","Filipe")

# print("Olá \t Filipe")

# print("Olá \n Filipe")

Greeting = "Hello"
name = "Filipe"

# print(Greeting+' '+name)

age = "29"

# print(Greeting + " " + name)

# print("You have", age, "years old.")

# print("You have" + " " + age + " " + "years old.")

# print("Hello " * 5)

# Isto é um comentário, não será executado. 

age = 29

# print('%s is %d years old' % (name,age))
# print('{} is {} years old'.format(name, age))
print(f'{name} is {age} years old')