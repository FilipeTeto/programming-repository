# -*- coding: utf-8 -*-
"""
Created on Mon Oct 12 10:48:34 2020

@author: filipeteto
"""

#-------------------------------- Exercício 01 ------------------------------#

print(2)

print("Welcome to python course")

print('Welcome to python course')

print(8+5*13)

print("Python is fun", 31)

print("Hello" + "Filipe")

print("Hello"' '"Filipe")

print("Hello" + "Filipe")

print("Hello, Filipe")

print("Hello " + "Filipe")

print("Hello"+" "+"Filipe")

print("Hello"," ","Filipe")

print("Olá \t Filipe")

print("Olá \n Filipe")

Greeting = "Hello"
name = "Filipe"

print(Greeting+' '+name)

age = "29"

print(Greeting + " " + name)

print("You have", age, "years old.")

print("You have" + " " + age + " " + "years old.")

print("Hello " * 5)

# Isto é um comentário, não será executado. 

age = 29

print('%s is %d years old' % (name,age))
print('{} is {} years old'.format(name, age))
print(f'{name} is {age} years old')


#------------------------------ Exercício 2 ---------------------------------#


print("Please, enter your name:")
student_name = input()
print("Welcome to Python Course " + student_name)


#---------------------------- Exercício 3 -----------------------------------#


print("Insert three names:")
str1, str2, str3 = input().split()
print(str1, str2 ,str3)


#----------------------------- Exercício 5 ----------------------------------#


quantity = 3

totalMoney = 1000

price = 450

statement1 = "I have {1} dollars so I can buy {0} football for {2:.2f} dollars."
print(statement1.format(quantity,totalMoney, price))

