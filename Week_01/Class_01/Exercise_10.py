#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 12 15:48:31 2020

@author: filipeteto
"""


#------------------------------ Exercício 10 --------------------------------#


credit_str = "xxxx----xxxx----5555---"

new_credit_str = credit_str[credit_str.index('5'):credit_str.index('5') + len('5555')]

print(new_credit_str)