# -*- coding: utf-8 -*-
"""
Created on Mon Oct 12 10:48:34 2020

@author: filipeteto
"""

#--------------------------------- Exercise 01 ------------------------------#

print(2)

print("Welcome to python course")

print('Welcome to python course')

print(8+5*13)

print("Python is fun", 31)

print("Hello" + "Filipe")

print("Hello"' '"Filipe")

print("Hello" + "Filipe")

print("Hello, Filipe")

print("Hello " + "Filipe")

print("Hello"+" "+"Filipe")

print("Hello"," ","Filipe")

print("Olá \t Filipe")

print("Olá \n Filipe")

Greeting = "Hello"
name = "Filipe"

print(Greeting+' '+name)

age = "29"

print(Greeting + " " + name)

print("You have", age, "years old.")

print("You have" + " " + age + " " + "years old.")

print("Hello " * 5)

# Isto é um comentário, não será executado. 

age = 29

print('%s is %d years old' % (name,age))
print('{} is {} years old'.format(name, age))
print(f'{name} is {age} years old')


#------------------------------- Exercise 2 ---------------------------------#


print("Please, enter your name:")
student_name = input()
print("Welcome to Python Course " + student_name)


#----------------------------- Exercise 3 -----------------------------------#


print("Insert three names:")
str1, str2, str3 = input().split()
print(str1, str2 ,str3)


#------------------------------ Exercise 5 ----------------------------------#


quantity = 3

totalMoney = 1000

price = 450

statement1 = "I have {1} dollars so I can buy {0} football for {2:.2f} dollars."
print(statement1.format(quantity,totalMoney, price))

#------------------------------- Exercise 10 --------------------------------#


credit_str = "xxxx----xxxx----5555---"

new_credit_str = credit_str[credit_str.index('5'):credit_str.index('5') + len('5555')]

print(new_credit_str)


#------------------------------ Other Exercises------------------------------#

frase = "Lisbon is in Portugal"

# print(frase)

print(frase[0])
print(frase[1])
print(frase[2])
print(frase[3])
print(frase[14])
print(frase[19])

print(frase[0:6])

print(frase[0:9])



# Deixar em comentário o que o código faz
    # Permite relembrar o que foi feito

a = 12
b = 3

print(a + b)    #15
print(a - b)    #9
print(a * b)    #12
print(a / b)    #4.0
print(a // b)   #4 integer division arredonda para baixo
print(a % b)    # 0 modulo: the 

print(type(a)) # permite identificar o tipo de dado (str,int,float, etc)

import sys

print("\nInteger value information: ",sys.int_info)

print("\nMaximum size of an integer: ",sys.maxsize)