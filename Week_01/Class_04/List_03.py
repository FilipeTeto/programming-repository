#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 15 15:43:50 2020

@author: filipeteto
"""

# #-------------------------- Ficha de Exercícios 03 --------------------------#


# #----------------------------- Exercício 01 ---------------------------------#


(5 > 4) and (3 == 5)                    # False
not (5 > 4)                             # False   
(5 > 4) or (3 == 5)                     # True
not ((5 > 4) or (3 == 5))               # False
(True and True) and (True == False)     # False
(not False) or (not True)               # True


# #----------------------------- Exercício 02 ---------------------------------#

spam = 0
if spam == 10:  
      print('eggs')   # ----> level 1 of indentation
      if spam > 5:
            print('bacon')   # ----> level 2 of indentation
      else:
          print('ham')
      print('spam')
print('spam')

#   We identify blocks by the indentation levels, 
#   here we have 2 blocks of indentation.


#----------------------------- Exercício 03 ---------------------------------#

count1=0
count2=0
count3=0
count4=0
numlist=[2, 61, -1, 0, 88, 55, 3, 121, 25, 75]

for i in numlist:
    if i >=0 and i <= 25:
        count1 = count1+1
    elif i >=26 and i <=50:
        count2 = count2+1
    elif i >=51 and i <=75:
        count3 = count3+1
    elif i >= 75:
        count4 = count4+1
    else:
        continue
print('Amount of numbers between [0, 25]: ', count1)
print('Amount of numbers between [26, 50]: ', count2)
print('Amount of numbers between [51, 75]: ', count3)
print('Amount of numbers between [76, 100]: ', count4)


#------------------------------ Exercício 04 --------------------------------#


text_1="""Catching up Start with The Portuguese: The Land and Its People (3)
by Marion Kaplan (Penguin), a one-volume introduction ranging from 
geography and history to wine and poetry, and Portugal: A Companion 
History (4) by José H Saraiva (Carcanet Press), a bestselling writer
and popular broadcaster in his own country."""

print(text_1.index('broadcaster'))
print(text_1.istitle())
print(text_1.islower())
print(text_1.isupper())
print(text_1.lower())
print(text_1.upper())
print(text_1.isnumeric())
print(text_1.split())
print(text_1.replace('p','f'))
print(text_1.isprintable())


#------------------------------ Exercício 05 --------------------------------#


for i in range(17):
   print("{0:>2}: binary is {0:>05b}".format(i))




