# -*- coding: utf-8 -*-
"""
Spyder Editor

@author: filipeteto
"""


# --------------------------------- Exemplo 01 -------------------------------#


for i in [5, 4, 3, 2, 1, 0]:
    print(i)
print('Boom!')


# --------------------------------- Exemplo 02 -------------------------------#


friends = ['Ana', 'João', 'Filipe', 'Joana', 'Teresa']

for friend in friends:
    print("Congrats on your new job: ", friend)
print("Done")

# --------------------------------- Exemplo 03 -------------------------------#


print('Before')
for i in range(0, 81, 2):
    print(i)
print('After')


#--------------------------------- Exemplo 04 -------------------------------#
#Largest So Far Loop

largest_so_far= -1
print('Before', largest_so_far)
for the_num in [9, 41, 12, 3, 74, 14]:
    if the_num > largest_so_far:
        largest_so_far = the_num
    print(largest_so_far, the_num)
    
print('After', largest_so_far)    


#---------------------------------- Exemplo 05 ------------------------------#
#Counting Loop

loop_interaction = 0
print('Before', loop_interaction)
for thing in [9, 41, 12, 3, 74, 15]:
    loop_interaction = loop_interaction + 1
    print(loop_interaction, thing)
print('After', loop_interaction)


#---------------------------------- Exemplo 06 ------------------------------#
# Alterar True para False depois de encontrar 3

found = False
print('Before', found)
for value in [9, 41, 12, 3, 74, 15]:
    if value == 3:
        found == True
    print(found, value)
if found == True:
    break
print('After)', found)


#-------------------------------- Exemplo 07 --------------------------------#
# Smallest So Far - Starting with None

smallest= None
print('Before')
for value in [9, 41, 12, 3, 74, 15]:
    if smallest == None:
        smallest = value
    elif value < smallest:
        smallest = value
    print(smallest, value)
print('After', smallest)
        

#-------------------------------- Exercício 01 ------------------------------#

    
def upperOnly(s):
    onlyCaps = ""
    for char in s:
        if char.isupper() == True:
            onlyCaps += char
    return onlyCaps
print (upperOnly('The Best of made in Portugal – Hats, Soaps, Shoes,Tiles & Ceramics, Cork.')

#-------------------------------- Exercício 02-------------------------------#
