import { useState, useRef } from 'react';
import data from './data';
import Player from "./components/Player";
import Song from "./components/Song";
import Library from "./components/Library"
import Nav from "./components/Nav"
import "./styles/App.scss"


function App() {

  const [songs, setSongs] = useState(data());
  const [currentSong, setCurrentSong] = useState(songs[0]);
  const [isLibraryOpen, setIsLybraryOpen] = useState(false);
  const [isPlaying, setIsPlaying] = useState(false);
  const audioRef = useRef(null);


  const openLibraryHandler = (event) => {
    setIsLybraryOpen(!isLibraryOpen);
  }

  return (
    <div className={`App ${isLibraryOpen ? 'library-active' : ''}`}>
      <Nav
        openLibraryHandler={openLibraryHandler} />
      <Song
        currentSong={currentSong} />
      <Player
        currentSong={currentSong}
        songs={songs}
        setCurrentSong={setCurrentSong}
        setSongs={setSongs}
        isPlaying={isPlaying}
        setIsPlaying={setIsPlaying}
        audioRef={audioRef} />
      <Library
        songs={songs}
        isLibraryOpen={isLibraryOpen}
        setSongs={setSongs}
        currentSong={currentSong}
        setCurrentSong={setCurrentSong}
        setIsPlaying={setIsPlaying}
        isPlaying={isPlaying}
        audioRef={audioRef}
      />
    </div>
  );
}

export default App;
