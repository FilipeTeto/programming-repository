import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faMusic } from "@fortawesome/free-solid-svg-icons"

const Nav = ({ openLibraryHandler }) => {

    return (
        <nav>
            <h1>WAVES</h1>
            <button onClick={openLibraryHandler}>
                LIBRARY
                <FontAwesomeIcon icon={faMusic} />
            </button>
        </nav>
    )
}

export default Nav;