import { useRef, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlayCircle, faPauseCircle, faChevronCircleLeft, faChevronCircleRight } from "@fortawesome/free-solid-svg-icons";


const Player = ({
    currentSong,
    songs,
    setCurrentSong,
    setSongs,
    isPlaying,
    setIsPlaying,
    audioRef }) => {



    const animateTrackRef = useRef(null);
    const [songInfo, setSongInfo] = useState({
        current: 0,
        duration: 0
    })

    // 1. Criar estado songInfo;

    const animationPercentage = (songInfo.current / songInfo.duration) * 100;

    const onAudioPlayHandler = (event) => {
        if (isPlaying) {
            audioRef.current.pause();
            setIsPlaying(false);
        } else {
            audioRef.current.play();
            setIsPlaying(true);
        }


    }

    // 2. Criar um Event Handler;

    const onTimeUpdateHandler = (event) => {
        const current = event.target.currentTime;
        const duration = event.target.duration;
        // animateTrackRef.current.style = {
        //     transform: `translateX(${Math.round((current / duration) * 100)}%)`
        // }
        setSongInfo({
            current: current,
            duration: duration
        });
    }

    const onTimeChangeHandler = (event) => {
        audioRef.current.currentTime = event.target.value;
        setSongInfo({ ...songInfo, current: event.target.value })
    }

    const onEndedHandler = async (event) => {
        const songsCount = songs.length;
        const currentSongIndex = songs.findIndex((song) => {
            return song.id === currentSong.id;
        })
        const nextIndex = (currentSongIndex + 1) % songsCount;

        await setCurrentSong(songs[nextIndex]);
        notifyActiveLibraryHandler(songs[nextIndex]);
        setIsPlaying(true);
        audioRef.current.play();
    }


    const notifyActiveLibraryHandler = (nextPrev) => {
        const newSongs = songs.map((song) => {
            if (song.id === nextPrev.id) {
                return {
                    ...song,
                    active: true
                }
            } else {
                return {
                    ...song,
                    active: false
                }
            }
        });
        setSongs(newSongs);
    }


    // const onLoadedMetadataHandler = (event) => {
    //     const current = audioRef.current.currentTime;
    //     const duration = audioRef.current.duration;
    //     animateTrackRef.current.style = {
    //         transform:
    //     }


    const skipTrackHandler = async (forwards) => {
        const songsLength = songs.length;
        const currentSongIndex = songs.findIndex((song) => {
            return song.id === currentSong.id;
        })
        let nextIndex;
        if (forwards) {
            if (currentSongIndex === songsLength - 1) {
                nextIndex = 0;
            } else {
                nextIndex = currentSongIndex + 1
            }
        } else {
            if (currentSongIndex === 0) {
                nextIndex = songsLength - 1;
            } else {
                nextIndex = currentSongIndex - 1
            }

        }
        await setCurrentSong(songs[nextIndex]);
        notifyActiveLibraryHandler(songs[nextIndex]);
        setIsPlaying(true);
        audioRef.current.play();

    }


    const getTime = (time) => {
        return (
            Math.floor(time / 60) + ":" + ("0" + Math.floor(time % 60)).slice(-2)
        )
    }



    return (
        <div className="player">
            <div className="time-control">
                <p>{getTime(songInfo.current)}</p>
                <div className="track" style={{
                    backgroundImage: `linear-gradient(to right, ${currentSong.color[0]}, ${currentSong.color[1]})`
                }}>
                    <input type="range" min={0} max={songInfo.duration} value={songInfo.current} onChange={onTimeChangeHandler} />
                    <div className="animate-track" ref={animateTrackRef} style={{ transform: `translateX(${animationPercentage - 0.028 * animationPercentage}%)` }}>
                        <div className="animate-track-thumb"></div>
                    </div>

                </div>
                <p>{getTime(songInfo.duration)}</p>
            </div>
            <div className="play-control">
                <FontAwesomeIcon icon={faChevronCircleLeft} size="2x" onClick={() => skipTrackHandler(false)} />
                <FontAwesomeIcon icon={isPlaying ? faPauseCircle : faPlayCircle} size="2x" onClick={onAudioPlayHandler} />
                <FontAwesomeIcon icon={faChevronCircleRight} size="2x" onClick={() => skipTrackHandler(true)} />
                <audio
                    src={currentSong.audio}
                    ref={audioRef}
                    onTimeUpdate={onTimeUpdateHandler}
                    onLoadedMetadata={onTimeUpdateHandler}
                    onEnded={onEndedHandler}
                />
            </div>
        </div>
    );
}

export default Player