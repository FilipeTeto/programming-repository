import LibrarySong from './LibrarySong'

const Library = ({
    songs,
    isLibraryOpen,
    currentSong,
    setCurrentSong,
    setSongs,
    isPlaying,
    setIsPlaying,
    audioRef }) => {
    return (
        <div className={`library ${isLibraryOpen ? 'open' : ''}`}>
            <h2>LIBRARY</h2>
            <div className="library-songs">
                {
                    songs.map((song) => {
                        return <LibrarySong
                            key={song.id}
                            song={song}
                            songs={songs}
                            currentSong={currentSong}
                            setCurrentSong={setCurrentSong}
                            setSongs={setSongs}
                            isPlaying={isPlaying}
                            setIsPlaying={setIsPlaying}
                            audioRef={audioRef}
                        />
                    })
                }
            </div>
        </div>

    )
}

export default Library;