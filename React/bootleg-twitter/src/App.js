import { useState, useRef } from 'react';
import TweetList from './components/tweetList';
import { uuid } from 'uuidv4';

function App() {

    const inputRef = useRef(null);

    const [abc, setAbc] = useState("abc");


    const [tweets, setTweets] = useState([]);

    /*useEffect(() => {
        console.log("useEffect activated")
        inputRef.current.value = "";
    }, [tweets])*/

    const submitClickHandler = (event) => {
        event.preventDefault();
        setAbc("submit");
        console.log(inputRef.current.value);
        const tweetObject = {
            id: uuid(),
            text: inputRef.current.value,
            liked: false
        }
        inputRef.current.value = ""
        const newTweets = [...tweets, tweetObject];
        setTweets(newTweets);
    }

    const appStyles = {
        color: "white",
        backgroundColor: "aqua"
    }


    return (
        < div className="App" style={appStyles}>
            <form>
                <input type="text" placeholder="Write your tweet here..." ref={inputRef} />
                <button onClick={submitClickHandler} type="submit">{abc}</button>
            </form>
            <TweetList message="Tweets" tweets={tweets} setTweets={setTweets} />
        </div >
    );
}

export default App;