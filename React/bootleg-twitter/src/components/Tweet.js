import s from '../styles/Tweet.module.css'
import styled from 'styled-components'
import { useState } from 'react'

const Tweet = ({ tweet, tweets, setTweets }) => {

    const onRemoveHandler = (event) => {
        const newTweets = tweets.filter((tweetItem) => {
            return tweetItem.id !== tweet.id;
        })
        setTweets(newTweets);
    }

    const [like, setLike] = useState(false)

    const onLikeHandler = (event) => {
        // buscar indice do tweet
        setLike(!like);
    }

    const TweetContainer = styled.div`
        background-color: green
        `
    return (
        <div className={s.tweet} id={tweet.id}>
            <TweetContainer>
                <h3>{tweet.text}</h3>
                <p><i class={`far fa-heart ${like ? '' : 'hidden'}`}></i></p>
                <button onClick={onRemoveHandler}>Remove</button>
                <button onClick={onLikeHandler}>Like</button>
            </TweetContainer>
        </div>
    )
}

export default Tweet