#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  4 14:45:48 2020

@author: filipeteto
"""

#------------------------------ Exercício 1 ---------------------------------#

import numpy as np
from functools import reduce


def get_candidates(search_space):
    return np.setdiff1d(np.arange(1, 10), reduce(np.union1d, search_space))


def isSolution(mat):
    missing = get_missing(mat)

    if not had_missing(missing):
        return True

    missing_col = get_missing_col(missing)
    missing_row = get_missing_row(missing)

    search_space = (
            get_col(mat, missing_col),
            get_row(mat, missing_row),
            get_square(mat, missing_col, missing_row)
        )

    for candidate in get_candidates(search_space):
        mat[missing_row, missing_col] = candidate
        if isSolution(mat):
            return True

    mat[missing_row, missing_col] = 0
    return False


def get_col(mat, idx):
    return mat[:, idx].reshape(9)


def get_row(mat, idx):
    return mat[idx, :].reshape(9)


def get_square(mat, col, row):
    col = col // 3 * 3
    row = row // 3 * 3
    return mat[row:row+3, col:col+3].reshape(9)


def get_missing(mat):
    return np.where(mat == 0)


def had_missing(missing):
    return len(missing[0])


def get_missing_col(missing):
    return missing[1][0]


def get_missing_row(missing):
    return missing[0][0]


mat = np.array([
    [0,2,0, 5,0,1, 0,9,0],
    [8,0,0, 2,0,3, 0,0,6],
    [0,3,0, 0,6,0, 0,7,0],

    [0,0,1, 0,0,0, 6,0,0],
    [5,4,0, 0,0,0, 0,1,9],
    [0,0,2, 0,0,0, 7,0,0],

    [0,9,0, 0,3,0, 0,8,0],
    [2,0,0, 8,0,4, 0,0,7],
    [0,1,0, 9,0,7, 0,6,0]
])

isSolution(mat)
print(mat)


#------------------------------ Exercício 2 ---------------------------------#

j = 1

def main():
    a = 9
    if a % 2 == 0:
        a = 2
    else:
        a = 3
        
    print(fun1(2,4))

    for i in range(3):
        for j in range(3):
            print(fun1(a,i+j))
            
def fun1(a, b):
    p = 1
    for i in range(b):
        p = p * a
    return p+j

main()


#------------------------------ A)

"""
j é variável global e as restantes são locais
"""

#------------------------------ B)

"""
17
2
4
10
4
10
28
10
28
82
"""


#------------------------------ Exercício 3 ---------------------------------#

"""

Write a program that reads a sequence of whole numbers and saves them in a list. Then, the
program must read another integer A and the program must find two different position
numbers in the list whose multiplication is C and print them out. If there are no such numbers,
the program must print a corresponding message.
List = [1 2 3 7 11 13]
A = 77
Output: 7 and 11
A = 50
Output: values not found

"""

x = list(map(int, input("Insert several numbers separated by a space: ").split()))
y = int(input("Insert one whole number: "))
result=()
for i in x:
    for j in x:
        if i * j == y:
            result =(i,j)
        else:
            continue
if result != ():
    print(result[0], "and ", result[1], "multiplied are equal to the input: ", y)
else:
    print("Values not found.")









