#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  5 12:27:20 2020

@author: filipeteto
"""

import csv
x = []
y = []
a = []
b = []
with open("/Users/filipeteto/Desktop/programming-repository/Week_04/Files/DP_LIVE_04112020161547500.csv", newline = "") as csvfile:
    csv_reader = csv.reader(csvfile, delimiter = ",")
    for row in csv_reader:         
        if row[0]== "PRT" and row[2] == "BOY":            
            # print(', '.join(row))
            x.append(row[5])
            y.append(round(float(row[6])))
        elif row[0]== "PRT" and row[2] == "GIRL":
            # print(', '.join(row))
            a.append((row[5]))
            b.append(round(float(row[6])))
    x = sorted(set(x)) # Um ano de cada vez, de modo a evitar repetições
    z = list(map(lambda x,y: (x+y)/2, y, b)) # Sum Boy and Girl
    # print(x)
    # print(y) 
    # print(a)
    # print(b)
    # print(z)       
            
import matplotlib.pyplot as plt

plt.title("Médias de Portugal")
plt.xlabel("Ano")
plt.ylabel("médias")
plt.plot(x,z)
plt.plot(z, color= "green", marker = "*")
plt.show()


#----------------------------------------------------------------------------#

import csv
x = []
y = []
a = []
b = []

AUS_boy = []
AUS_girl =[]
BEL_boy=[]
BEL_girl=[]
CAN_boy=[]
CAN_girl=[]
CZE_boy=[]
CZE_girl=[]
DNK_boy=[]
DNK_girl=[]

with open("/Users/filipeteto/Desktop/programming-repository/Week_04/Files/DP_LIVE_04112020161547500.csv", newline = "") as csvfile:
    csv_reader = csv.reader(csvfile, delimiter = ",")
    for row in csv_reader:         
        if row[0]== "PRT" and row[2] == "BOY":            
            #print(', '.join(row))
            x.append(row[5])
            y.append(round(float(row[6])))
        elif row[0]== "PRT" and row[2] == "GIRL":
            #print(', '.join(row))
            a.append((row[5]))
            b.append(round(float(row[6])))            
        elif row[0]== "AUS" and row[2] == "BOY":            
            #print(', '.join(row))            
            AUS_boy.append(round(float(row[6])))
        elif row[0]== "AUS" and row[2] == "GIRL":
            #print(', '.join(row))            
            AUS_girl.append(round(float(row[6])))
        elif row[0]== "BEL" and row[2] == "BOY":            
            #print(', '.join(row))            
            BEL_boy.append(round(float(row[6])))
        elif row[0]== "BEL" and row[2] == "GIRL":
            #print(', '.join(row))            
            BEL_girl.append(round(float(row[6])))
        elif row[0]== "CAN" and row[2] == "BOY":            
            #print(', '.join(row))            
            CAN_boy.append(round(float(row[6])))
        elif row[0]== "CAN" and row[2] == "GIRL":
            #print(', '.join(row))            
            CAN_girl.append(round(float(row[6]))) 
        elif row[0]== "CZE" and row[2] == "BOY":            
            #print(', '.join(row))            
            CZE_boy.append(round(float(row[6])))
        elif row[0]== "CZE" and row[2] == "GIRL":
            #print(', '.join(row))            
            CZE_girl.append(round(float(row[6]))) 
        elif row[0]== "DNK" and row[2] == "BOY":            
            #print(', '.join(row))            
            DNK_boy.append(round(float(row[6])))
        elif row[0]== "DNK" and row[2] == "GIRL":
            #print(', '.join(row))            
            DNK_girl.append(round(float(row[6])))                        

    x = sorted(set(x)) # Um ano de cada vez, de modo a evitar repetições
    PRT_media = list(map(lambda x,y: (x+y)/2, y, b)) #p Sum Boy and Girl
    AUS_media = list(map(lambda x,y: (x+y)/2, AUS_boy, AUS_girl))
    BEL_media = list(map(lambda x,y: (x+y)/2, BEL_boy, BEL_girl))
    CAN_media = list(map(lambda x,y: (x+y)/2, CAN_boy, CAN_girl))
    CZE_media = list(map(lambda x,y: (x+y)/2, CZE_boy, CZE_girl))
    DNK_media = list(map(lambda x,y: (x+y)/2, DNK_boy, DNK_girl))
    # print(x)
    # print(y) 
    # print(AUS_boy)
    # print(AUS_girl)
    # print(AUS_media)  
     
           
            
import matplotlib.pyplot as plt

plt.title("Comparação de 5 paises com Portugal")
plt.xlabel("Ano")
plt.ylabel("médias")
plt.plot(x,PRT_media)
plt.plot(PRT_media, color= "green", marker = "*")
plt.plot(AUS_media, color ="orange", marker = "*")
plt.plot(BEL_media, color ="yellow", marker = "*")
plt.plot(CAN_media, color ="red", marker = "*")
plt.plot(CZE_media, color ="purple", marker = "*")
plt.plot(DNK_media, color ="blue", marker = "*")
plt.show()

#----------------------------------------------------------------------------#


import csv
# x = []
y = []
b = []
b2006= []
g2006= []
b2009= []
g2009= []
b2012= []
g2012= []
b2015= []
g2015= []
b2018= []
g2018= []
bo = []
gi = []

with open("/Users/filipeteto/Desktop/programming-repository/Week_04/Files/DP_LIVE_04112020161547500.csv", newline = "") as csvfile:
    csv_reader = csv.reader(csvfile, delimiter = ",")
    for row in csv_reader:     
        if row[2] == "BOY" and row[5] =="2003":            
            #print(', '.join(row))
            x.append(row[5])
            y.append(round(float(row[6])))
            y1 = sum(y)  
        elif row[2] == "GIRL" and row[5] =="2003":
            #print(', '.join(row))            
            b.append(round(float(row[6])))
            b1 = sum(b)
        elif row[2] == "BOY" and row[5] =="2006":            
            #print(', '.join(row))
            x.append(row[5])
            b2006.append(round(float(row[6])))
            b06=sum(b2006)
        elif row[2] == "GIRL" and row[5] =="2006":
            #print(', '.join(row))            
            g2006.append(round(float(row[6]))) 
            g06=sum(g2006)
        elif row[2] == "BOY" and row[5] =="2009":            
            #print(', '.join(row))
            x.append(row[5])
            b2009.append(round(float(row[6])))
            b09=sum(b2009)
        elif row[2] == "GIRL" and row[5] =="2009":
            #print(', '.join(row))            
            g2009.append(round(float(row[6]))) 
            g09=sum(g2009)
        elif row[2] == "BOY" and row[5] =="2012":            
            #print(', '.join(row))
            x.append(row[5])
            b2012.append(round(float(row[6])))
            b12=sum(b2012)
        elif row[2] == "GIRL" and row[5] =="2012":
            #print(', '.join(row))            
            g2012.append(round(float(row[6]))) 
            g12=sum(g2012)
        elif row[2] == "BOY" and row[5] =="2015":            
            #print(', '.join(row))
            x.append(row[5])
            b2015.append(round(float(row[6])))
            b15=sum(b2015)
        elif row[2] == "GIRL" and row[5] =="2015":
            #print(', '.join(row))            
            g2015.append(round(float(row[6]))) 
            g15=sum(g2015)
        elif row[2] == "BOY" and row[5] =="2018":            
            #print(', '.join(row))
            x.append(row[5])
            b2018.append(round(float(row[6])))
            b18=sum(b2018)
        elif row[2] == "GIRL" and row[5] =="2018":
            #print(', '.join(row))            
            g2018.append(round(float(row[6]))) 
            g18=sum(g2018)
            
    bo.append(y1)     
    bo.append(b06)  
    bo.append(b09)  
    bo.append(b12)  
    bo.append(b15)  
    bo.append(b18)  
   
    gi.append(b1)     
    gi.append(g06)  
    gi.append(g09)  
    gi.append(g12)  
    gi.append(g15)  
    gi.append(g18)     
   
x = sorted(set(x))
# print(x)            
y1 = sum(y)            
b1 = sum(b)
b06=sum(b2006)
g06=sum(g2006)
b09=sum(b2009)
g09=sum(g2009)
b12=sum(b2012)
g12=sum(g2012)
b15=sum(b2015)
g15=sum(g2015)
b18=sum(b2018)
g18=sum(g2018)


import matplotlib.pyplot as plt
import numpy as np
barWidth = 0.20

r1 = np.arange(len(x))
r2 = [i + barWidth for i in r1]
r3 = [i + barWidth for i in r2]

plt.title("Girl and Boys Comparison")
plt.xlabel("Ano")
plt.ylabel("Total")
plt.legend()
plt.bar(r1,bo, color="blue")
plt.bar(r2,gi, color = "pink")
plt.xticks([r for r in range(len(x))],x)
plt.show
