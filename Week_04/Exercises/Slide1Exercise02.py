#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  2 15:11:25 2020

@author: filipeteto
"""
"""
Write a function that takes as a parameter a list of integers. The function must
return a tuple with two integer values f1 and f2, where f1 is the list element with
lowest frequency (lowest number of occurrences in the list) and f2 is the element with
the highest frequency. Tip: use a dictionary to compute the frequencies of the list
elements.
The following function must be implemented:

def frequencies (v)

Input = [1, 4, 5, 1, 6, 3, 2, 1, 2, 9, 1, 4, 6, 3, 9, 7]

Output = ([f1], [f2]) f1 = [5,7] f2=[1]
"""


v = [1, 4, 5, 1, 6, 3, 2, 1, 2, 9, 1, 4, 6, 3, 9, 7]

print("Original list: ",str(v))

def frequencies(v):
    f1 = []
    f2 = []
    dict1 = {}
    for i in v:
        if i not in dict1:
            dict1[i] = 1
        else:
            dict1[i] += 1
    highest_freq = 0
    minimum_freq = len(v)
    for i in dict1.values():
        if i > highest_freq:
            highest_freq = i 
        elif i < minimum_freq:
           minimum_freq = i 
        else:
            pass
    for i in dict1.items():
        if i[1] == highest_freq:
            f1.append(i[0])
        elif i[1] == minimum_freq:
            f2.append(i[0])
        else:
            pass
    print("The highest frequency is in:", f1)
    print("The lowest frequency is in: ",f2)
    
frequencies(v)