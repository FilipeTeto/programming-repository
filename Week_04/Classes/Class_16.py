#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  2 08:33:04 2020

@author: filipeteto
"""

#--------------------------------- Aula 16 ----------------------------------#

"""
Repository organization should be:
    
week 1_
       classes_
               códigos dos slides
               aula 15.py
               aula 16.py
               aula 17.py
               aula 18.py
       lists___
               list_1
               list_2
       exercises_
               exer1.py
               exer2.py
               
"""
#----------------------------------------------------------------------------#

"""
Importing modules
Finding function attributes
Documentation
List: <identification> = [<value1>, <value2>]
    Accepts different variables types
    Index to access and/or find members
    cannot change strings in lists using indexes
    Ordered
    usually for loop is used to manipulate lists
    it is possible to make slicing
    it is possible to concatenate lists with * and +
    in and in not indicates membership
    it is possible to include a list inside other list
    Lists have methods such as .pop(), insert(), count(), append()
    Filtering
"""

import sqlite3
from sqlite3 import Error
from numpy import *
import numpy as np

b = [4, 6, 7]
print(np.add(b, 5))

print(dir(np))

L = [3, True, "ali", 2.7, [5, 8]]
a = [3, [109, 27], 4, 15]
# print(a(1))
print(a[1])

a = [7, 5, 40, 2, 6, 25]
print(a[1:4])

a = [1, 3, 6, 5, 3]
print(a.count(3))

a = []
for i in range(4):
    a.append(i)
print(a)

#----------------------------------------------------------------------------#

"""
Tuple:  <identification> = (<value 1>, <value 2>)
        index to access and /or find members
        cannot change strings in tuples using indexes
        it is possible to caoncatenate lists with * and +
        Ordered
        Lists have methods such as .count(), .index()
        It is possible to use python build in fucntions to manipulate tuples
        Removing member is only possible by converting to another type
        it is possible to save tuple member in other variables - unpacking
        A look in .zip()
"""

t = ("English", "History","Mathematics")
print(t[1])
print(t.index("English"))

t = (1, 9, 3, 9)
print(t.count(9))
print(max(t))

#----------------------------------------------------------------------------#

"""
Dictionary: <identiifcation> = {<value1> : <value2>, <value3> : <value4>}
            add valuels in dictionary
            lists have methods such as .get(), .pop(), .popitem(), .copy(),
            update().
            it is possible to use python built in fucntions to manupulate dictionaries sum()
            sorting using operator module


d = {"brand": "cherry", "model": "arizo5", "color":"white"}
d['color'] = "black"
print(d)

x = d.get("model")
print(x)

print(list(d.keys))
print(list(d.values)) 


d.pop("model")
print(d) 
"""
#----------------------------------------------------------------------------#

"""
Sets: <identification> = {<value1>,<value2>,<value3>}
      add values in the set
      have methods such as .add(), .update(), .remove(), .copy(), .update()
      Joint theory
      Subset
"""      


#----------------------------------------------------------------------------#

"""
Files: open files open(), . close()
       open(<path to the file>, <mode>)
       with open takes care of closing files
"""

#----------------------------------------------------------------------------#

epopeia = open("/Users/filipeteto/Desktop/programming-repository/Week_04/Files/Epopeia.txt")

for line in epopeia:
    print(line)
    if "tempestade" in line.lower():
        print(line)

epopeia.close()

with open ("/Users/filipeteto/Desktop/programming-repository/Week_04/Files/Epopeia.txt") as epopeia:
    for line in epopeia:
        if "tempestade" in line.lower():
            print(line)
print(line)

#----------------------------------------------------------------------------#
"""
Files:
      manipulating files .read(), .readline(), .readlines()
      readline() - reads a single line of line
      readlines() - reads the entire file and return a list of strings
      read() - reads the whole file and returs a string
"""
#----------------------------------------------------------------------------#
encoding_type = "utf-8"
file_path = "/Users/filipeteto/Desktop/programming-repository/Week_04/Files/Epopeia.txt"
epopeia = open (file_path, 'r')
# for line in epopeia:
#     if "tempestade" in line.lower():
#         print(line)
# epopeia.close()

with open (file_path, 'r', encoding=encoding_type):
    for line in epopeia:
        if "tempestade" in line.lower():
            print(line)

#----------------------------------------------------------------------------#

encoding_type = "utf-8"
file_path = "/Users/filipeteto/Desktop/programming-repository/Week_04/Files/Epopeia.txt"

with open(file_path,'r',encoding=encoding_type) as epopeia:
    line = epopeia.readline()
    while line:
        print(line, end='')
        line = epopeia.readline()
    
with open(file_path,'r',encoding=encoding_type) as epopeia:
    poem = epopeia.readlines()
    print(type(poem))
print(poem)

with open(file_path,'r',encoding=encoding_type) as epopeia:
    text = epopeia.read()
    print(type(text))
print(text)

#----------------------------------------------------------------------------#

"""
Files: Json files - JavaScript Object Notation file - easy to understand by human 
                                                      and machines 
                                                      
      Data separated by commas;
      Curly braces hold objects;
      Square brackets hold array;
      
https://jsonlint.com/?code=
"""  

import json

with open("/Users/filipeteto/Desktop/programming-repository/Week_04/Files/dados.json") as json_file:
    json_object = json.load(json_file)
    name = json_object['data'][1]['name']
    address = json_object['data'][1]['address']['postal_code']
# 
print(json_object)
print(name)
print(address)


a= json.dumps(json_object, indent= 4)
print(a)

#----------------------------------------------------------------------------#

import pandas as pd
a = "/Users/filipeteto/Desktop/programming-repository/Week_04/Files/Employee_data.xlsx"
all_data = pd.read_excel(a, index_col=1)
email = all_data['company_name']
xl = pd.ExcelFile(a)
df = xl.parse("b")


















