#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  5 08:40:12 2020

@author: filipeteto
"""

#----------------------------------- Pandas  --------------------------------#

import pandas as pd
print(pd.__version__)

data = pd.Series([0.25, 0.5,0.75,1])
print(data)
print(data.values)
print(data.index)
print(data[0])
data = pd.Series([0.25, 0.5,0.75,1.0],
                 index = ['a','b','c','d'])
print(data)

population_dict = {'California':38332521,
                   'Texas':26448193,
                   'New York':19651127,
                   'Florida':19552860,
                   'Illinois':12882135}

population = pd.Series(population_dict)
print(population)
print(population['California':'Florida'])



a = pd.Series([2, 4, 6])
print(a)

b = pd.Series(5, index =[100,200,300])
print(b)

c = pd.Series({2:'a',1:'b',3:'c'})
print(c)

c = pd.Series({2:'a',1:'b',3:'c'}, index = [3,2])
print(c)


#--------------------------------- Dataframe  -------------------------------#


area_dict = {'California':423967,'Texas':695662,'New York':131297,
                   'Florida':170312,'Illinois':149995}

area = pd.Series(area_dict)
print(area)

states = pd.DataFrame({'Population':population, 'Area':area})
print(states)
print(states.index)
print(states.columns)
print(states['Area'])
print(pd.DataFrame(population, columns=['Population']))
data = [{'a':i, 'b':2*i}
        for i in range(3)]
print(pd.DataFrame(data))
print(pd.DataFrame({'Population':population,'Area':area}))


#--------------------------- Pandas Index Object  ---------------------------#


ind = pd.Index([2,3,5,7,11])
print(ind)
print(ind[1])
print(ind[::2])
print(ind.size, ind.shape, ind.ndim, ind.dtype)
# ind[1] = 0 não é permitido mudar valores depois de criar o index no pandas


