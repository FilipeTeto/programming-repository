#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  4 08:58:42 2020

@author: filipeteto
"""

#---------------------------------- Numpy -----------------------------------#

"""
NumPy is the fundamental package needed for scientific computing with Python

Nympy Features:
               Typed multidimensional arrays (vector,matrices)
               Fast numerical computations (matrix,math)
               High level math functions
               Open Source
"""
import numpy as np

print(np.__version__)
print(np.version)

students = np.array([[[1,3,5],[1,1,1]],
                      [[4.5,4,5],[4.3,4.4,4.6]]])

print(students)
print(students.shape,"\n", 
      students.nbytes,"\n",
      students.ndim, "\n",
      students.dtype, "\n",
      students.size, "\n",
      students.data, "\n",
      students.itemsize)

x3 = np.power(10,4,dtype=np.int8) # 16 overflow int8 size
print(x3)
x4 = np.power(10,4,dtype=np.int16) # 10000 correct value
print(x4)

"""
np.int8 - byte (-128 to 127)
np.int16 - integer (-32768 to 32767)
np.int32 - integer (-2147483648 to 2147483647)
np.int64 - integer (-9223372036854775808 to 9223372036854775807)
"""

#---------------------------- Reshape vs Resize -----------------------------#


print(np.arange(10))
print(np.arange(10).reshape(2,5))

print(np.resize(np.arange(10),(2,5)))
print(np.resize(np.arange(10),(2,7)))


d = np.arange(2,5) #caso fosse feito print sem newaxis, seria um vetor linha
print(d[:,np.newaxis]) # cria um vetor coluna
print(d[:,np.newaxis].shape)


#---------------------------------- Copy ------------------------------------#


x = np.array([1,4,3])
y = x
z = np.copy(x)
x[1] = 2
print(x)
print(y)
print(z)


#---------------------------------- Sort ------------------------------------#

dtype = [('name','S12'),('grade',float), ('age',int)]
values = [('Joseane',5,31),('Hamed',5,32),('Stefan',5,24)]
sorted_data = np.array(values,dtype=dtype)
print(np.sort(sorted_data, order='age'))

for i in range(5):
    for j in range(6):
        print('Josi')
    print('Filipe')


#-------------------- Creating Zeros Vectors and Matrices -------------------#

b = np.zeros((2,2,3),dtype=int)
print(b)
print(b.ndim, b.dtype, b.size)

b = np.zeros((10,10))
print("%d bytes"% (b.size * b.itemsize))
b[8] = 1
print(b)

b = np.arange(50)
b = b[::-1]
b = b.reshape(5,10)
print(b)
print(b[4,1])
print(b[3,:])
print(b[0,2:])
print(b[:1,1:])

"""
x[0,0]  - Top left element
x[0,-1] - First row, last column

"""


#--------------------- Concatenation, Vstack and Hstack  --------------------#

# print(np.ones((10)))
b = np.arange(50)
b = b.reshape(5,10)
c = np.ones((2,10))
# print(np.concatenate((b,c)))
d = np.hstack(c)
print(np.vstack(d))


#--------------- Ones, zeros_like, ones_like, eye, empty, full --------------#

d = np.ones((1,3))
x = np.zeros_like((d))
x1 = np.ones_like((x))
print(d)
print(x)
print(x1)


b = np.eye(3)
print(b)

print(np.full((2,1), np.inf))

a = np.full((2,1), np.inf)
print(a)
print(a.dtype, (a.size * a.itemsize))
print(np.empty((2,1)))

b = np.empty((2,1))
print(b.size * b.itemsize)


#-------------------------- Useful Functionalities --------------------------#

yesterday = np.datetime64('today') - np.timedelta64(1)
today = np.datetime64('today')
tomorrow = np.datetime64('today') + np.timedelta64(1)

print(yesterday, today, tomorrow)
b = np.arange('2019', '2021',dtype = 'datetime64[D]')
print(b.size, b.ndim)


#---------------------------- Numpy and Matplotlib --------------------------#

import matplotlib.pyplot as plt

values = np.linspace(-(2*np.pi),2*np.pi,50)
cos_values = np.cos(values)
sin_values = np.sin(values)

plt.plot(cos_values, color = 'blue', marker = '*')
plt.plot(sin_values, color = 'red', marker = '*')
plt.show()

#------------------------------- Example 01 ---------------------------------#
import numpy as np
x = np.array([[-1,3,],[4,2]])
y = np.array([[1,2],[3,4]])
z = np.dot(x,y)

print(z)


