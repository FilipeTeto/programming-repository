#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  5 11:05:41 2020

@author: filipeteto
"""

#--------------------------- Git Revert vs Reset ----------------------------#

"""
Git Revert - Creates a new commit with the previous commit that you are reverting to.

Git Reset - Allows you to go back to a previous commit and removes the other commits on it's
            way back.
"""