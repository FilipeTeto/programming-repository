#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 19 08:37:42 2020

@author: filipeteto
"""

#---------------------------------- Aula 6 ----------------------------------#

# _name_2625 = 'Filipe'

# print(_name_2625)

# _name_2625 = 29

# print(_name_2625)


# from keyword import iskeyword

# print(iskeyword('if'))

# print('a2'.isidentifier())
# print('2a'.isidentifier())
# print('my_var'.isidentifier())


# a = 12
# b = 15

# a, b = b, a

# print(a)
# print(b)

# import math
# n= -16
# if n < 0:
#     n=abs(n)

# print(math.sqrt(n))

# a = 5
# if True:
#     a =6
# print(a)

# a = 17

# if a % 2 == 0:
#     print('even')
# else:
#     print('odd')    
    
# a=2
# b=5

# m = a if a < b else b

# print(m)

# my_list: ['a', 'e', 'i' , 'o','u']

# # if 'o in my_list':
# #     s = 'yes'
# # else:
# #     s= no
    
# s = 'yes' if ('o' in my_list) else 'no'
    
# print(s)    

# print(list(range(12)))

# s = 'Python'

# for ch in s:
#     print(ch)

# for _ in range(3):
#     print('hello', end=' ')

# word = 'alireza'
# c=0
# for i in word:
#     if i == 'a':
#         c+=1
#     print(c)


# m= []
# name = 'farshid joe sara'         #conta a quantidade v dentro de name
# v= "aeiou"

# for ch in name:
#     if ch in v:
#           m.append(ch)
    
          
# print(set(m))        


# name = 'farshid'
# v = 'aeiou'
# a = [ch for ch in name if ch in v]
# print(a)

# for i in range(1, 4):
#     for j in range(2, 4):
#         print(j, end=' ')
#     print()

# for i in range(5):
#     if i == 3:
#         break
#     else:
#         print(i,end=' ')
        
# for i in range(5):
#     if i == 3:
#         continue
#     else:
#         print(i,end=' ')

# i = 1
# while i <= 3:
#     print(i,end=' ')
#     i += 1

# n = 7
# while n >= 3:
#     print(n, end= ' ')
#     n -= 1

# s='abcdef'
# i = 0
# while True:
#     if s[i] == 'd':
#         break
#     print(s[i], end=' ')
#     i += 1

# n = 8
# while n > 2:
#     n -= 1
#     if n ==5:
#         break
#     print(n, end= ' ')
    
# n = 8
# while n > 7:
#     n -= 1
#     if n == 5:
#         continue
#     print(n, end=' ')

