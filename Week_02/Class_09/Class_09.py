#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 22 08:33:23 2020

@author: filipeteto
"""

#-------------------------------- Tuples ------------------------------------#

t = ('English','History', 'Mathematics')
print(t)
print(type(t))
print(len(t))

t1 = (3)    # t1 is integer
t2 = (3,)   # t2 is tuple
s1 = ('a')  # s1 is string
s2 = ('a',) # s2 is tupple

# # We need to put parenthesis in the variable to become a tuple if it is only 
# # one value.


# ## To access the members in tuple we use [], same as list

print(t[0])
print(t[1:3])
print(t.index('English'))
print('English' in t)

for i in t:
    print(f'I like to read {i}')
    

a = (1, 2, 3)
    
for i in a:
    i = a[0]
    i = a[1]
    i = a[2]
    ...
    
if i in a:              # we use or for conditions, not 'and'.
  if(i == a[0] or i == a[1] or i == a[2]):
    ...

t= (1, 9, 2)

print(sum(t))
print(max(t))
print(min(t))
print(t.count(9))


# # * and + in tupples

print(t*2)
print(t+t+t)
print((3,6)+(9,))
print((1,2)+(9,6))

print(tuple(reversed(t)))


#------------------------------ Exercise 01 ---------------------------------#

t = (4, 6)
print(t)
l = list(t)
l.append(9)
t = tuple(l,)
print(t)


#------------------------------ Exercise 02 ---------------------------------#

# My option

t = "$ $ Python$#Course  $"
l = list(t)

b =[i.strip('$ #') for i in l]
b.insert(b.index('C'), ' ')
b = ''.join([str(i) for i in b])
print(b)

# # Option 2

t = "$ $ Python$#Course  $"
a = t.strip('# $')
a = list(t)
b = a.copy()
for i in a:
    if i == "#" or i == "$" or i == " " : # i in ["#", "$", "\n", "\t]
        b.remove(i)

b.insert(b.index('C')," ")
t=''.join(b)
print(t)


#----------------------------------------------------------------------------#


# Unpacking

t = (4, 8)
a, b = t
print(a)
print(b)

car =('blue', 'auto', 7)
color, _, a = car
print(color)
print(_)
print(a)


# Zip

a = (1, 2) 
b = (3, 4)
c = zip(a,b)
x = list(c)
print(x)
print(x[0])
print(type(x[0]))

z = ((1, 3), (2, 4))
u = zip(*z)
print(list(u))

a = (1, 2, 'A')
b = (3, 4, 8)
c = zip(a,b)
x = list(c)

print(x)
print(list(zip(*x)))


# minimum length between 'a' and 'range(2)' 

a= [11, 22, 33]
b = zip(a, range(2))
print(list(b))


#------------------------------- Exercise 03 --------------------------------#

"""
Try Zip with 3 input variables

a= [1, 2, 'a']
b = ('Python', 161.8, 0, 5)
c = {10, 12, 14, 16, 18, 20}

Output ==> [(1, 'Python', 10), (2, 161.8, 12), ('A', 0, 14)]

"""

A = [1, 2, "A"]
B = ('Python', 161.8, 0, 5)
C = {10, 12, 14, 16, 18, 20}

print(list(zip(A, B, C)))

minABC =min(len(A), len(B), len(C))
            
C=list(C)
i = 0
D1 = [(A[i], B[i], C[i]), (A[i+1], B[i+1], C[i+1]), (A[i+2], B[i+2], C[i+2])]
D2 = [(A[0], B[0], C[0]), (A[1], B[1], C[1]), (A[3], B[3], C[3])]

e =[]
for i in range(minABC):
    my_tuple=(A[i],B[i],C[i])
    e.append(my_tuple)

output= e
print(output)    


#----------------------------------------------------------------------------#

# how many tuples are in num list

num = [8, 2, (9, 3), 4, (1, 6, 7), 34]
c= 0
for i in num:
    if isinstance(i, tuple):
        continue
    c += 1
print(c)
print(len(num) - c)

c= 0
for i in num:
    if type(i) == tuple:
        continue
    c += 1
print(c)
print(len(num)-c)


#----------------------------------------------------------------------------#
"""
Input:  [(1, 2, 3), (4, 5, 6)]
Output: [(1, 2, 9), (4, 5, 9)]
"""

a = [(1, 2, 3), (4, 5, 6)]
b = [i[:-1]+(9,) for i in a]
print(b)

"""
Homework try this in another way
"""

        














