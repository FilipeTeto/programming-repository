#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 22 13:01:03 2020

@author: filipeteto
"""

#------------------------------- Exercise 01 --------------------------------#


a = [(1, 2, 3), (4, 5, 6)]
c = a.copy()
for i in range(len(a)):
    c[i] = a[i][0],a[i][1], 9
print(c)


#------------------------------- Exercise 02 --------------------------------#


my_obj = enumerate(['Batata','Mandioca'])

for i, j in my_obj:
    print("i is: ", i)
    print("j is: ", j)
    

#------------------------------- Exercise 03 --------------------------------#


"""
Input:  [(1,3), (2,4), ('A',8)]

Output: [(1, 2, 'A'), (3, 4, 8)]
"""

received = [(1,3), (2,4), ('A',8)]

A, B, C = received
toList = []
for  i in range(len(received)-1):
    D =[(A[i],B[i],C[i])]
    toList.append(D)
print(toList)
    