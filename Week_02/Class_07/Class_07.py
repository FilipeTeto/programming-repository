
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 20 08:33:55 2020

@author: filipeteto
"""

#--------------------------------- Aula 07 ----------------------------------#

# Most frequent used modules
# Time , Sys, os, math, random, pickle, urllib, re, cgi, socket

import math as m                  # or import math
print(m.pi)                       # print(math.pi)

# Always import numpy as np - easier for everyone 
#                             to know what module you're using

from os import getcwd as gc       # From module import submodule/funcions/variables as ***
#                                   # or from module import submodule/funcions/variables 
print(gc())

# import numpy as np
# print(np.array([1]))

from math import pi

print(pi)

# print(math.pi)

# print(math.e)

# math(e)

import numpy as np
# import pandas as pd
from matplotlib import pyplot as plt
# import matplotlib.pyplot as plt
data = np.array([-20, -3, -2, -1, 0, 1, 2, 3, 4])
plt.boxplot(data)

print(dir(m))

s = 'a'
print(dir(s))



import datetime

now = datetime.datetime.now()

print(now)

print(now.year)

print(now.month)

print(now.day)

print(datetime.datetime.today())
