#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 20 11:15:20 2020

@author: filipeteto
"""

#------------------------------ Exercise 01 ---------------------------------#
import math

print(math.fmod(9,4)) # = 9%4

print(math.gcd(30,4)) # greatest common diviser

print(math.fabs(-4)) # float absolut of a number

import random

print(random.randint(1,5)) # random number between 1 to 5

print(random.choice([1,5])) # random choice between only 1 or 5

a = [1, 2, 3, 4]

print(random.shuffle(a)) # randmize the arrangement of a


#------------------------------- Exercise 02 --------------------------------#

import sys

print(sys.version)

print(sys.platform)

import platform

print(platform.mac_ver())


#-------------------------------- Exercise 03 -------------------------------#


import datetime

now = datetime.datetime.now()



print(now.minute)

if now.minute %2:
    
    print('Odd minute')
    
else:
    
    print('Not an odd minute')


"""
For loop to get even or odd

List to get even or odd
"""

for i in range(0,60,2):
    
    if now.minute == i:
    
        print('This is even')
        break
    else:
        print('This is odd')
        break


odds_list:[i for i in range(1,60,2)]

if now.minute == i:
    print('This is even')

else:
    print('This is odd')
        
