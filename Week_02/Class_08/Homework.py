#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 21 15:33:28 2020

@author: filipeteto
"""

m    = [
         [1, 2, 3],
         [4, 5, 6],
         [7, 8, 9]
        ]

"""
1 - Print first row
2 - Print second column in a single line
3 - Print main diameter 1, 5, 9
4 - Print another diameter 3, 5, 7
5 - Calculate sum of rows
6 - Calculate sum of columns
"""

#-------------------------------- Exercise 01 -------------------------------# 


print(m[0])


# #-------------------------------- Exercise 02 -------------------------------# 


b = []
for i in m[0]:
    if i == 2:
        b.append(i)
for i in m[1]:
    if i == 5:
        b.append(i)
for i in m[2]:
    if i == 8:
        b.append(i)
print(b)


# #-------------------------------- Exercise 03 -------------------------------# 


c=[]
l = len(m[0])
for i in range(l):
    c.append(m[i][i])      
print(c)


# #-------------------------------- Exercise 04 -------------------------------# 

l = len(m[0])
d=[]
for i in range(l-1,-1,-1):      
    d.append(m[l-1-i][i])
print(d)


# #-------------------------------- Exercise 05 -------------------------------# 


e=[sum(i) for i in m]
print(e)


#-------------------------------- Exercise 06 -------------------------------# 

column_sums = [sum([row[i] for row in m]) for i in range(0,len(m[0]))]
print(column_sums)

#----------------------------- Bonus Exercise -------------------------------# 

for row in m:
    print(row)
print("\n")
t_m = zip(*m)
for row in t_m:
    print(row)

m2 = []

for i in range(3):
    m2.append([0,0,0])

for i in range(3):
    for j in range(3):
        m2[j][i] = m [i][j]
        
print(m)      
print(m2)