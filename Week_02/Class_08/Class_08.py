#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 20 20:25:30 2020

@author: filipeteto
"""

#-------------------------------- Aula 08 -----------------------------------#

# # #List

# a= [5, 7, 12]       # define a list
# print(a[0])         # 5
# print(a[1])         # 7
# print(a[2])         # 12
# print(type(a))      # <class 'list'>
# print(len(a))       # 3

# # You can place different types in a list

# b =[1.618, 'Python Course', 0]

# # Methods and attributes of a List

#   # Simply use dir()


# # Compare list with a string (the only difference is that the string is immutable,)



# friends = ['Hamed', 'Josi', 'Stefan']
# for f in friends:
#     print(f)
    
# for i in range(3):
#     print(friends[i])
 
# my_list = [1, 2, 23, 4, 'word']
# for i in [0, 1, 2, 3, 4]:
#     if i == len(my_list)-1:
#         break
#     print(my_list[i],my_list[i+1])
    

# for i in range((len(my_list)-1)):
    # print(my_list[i], my_list[i+1])
    
    
# Slicing a list
    
# alist=[7, 5, 30, 2, 6, 25]

# print(alist[1:4]) # start to stop with step +1(default)

# print(alist[:3]) # start from beginning, stop and with default step

# print(alist[3:]) #start from index(3) and stop in the end, default step

# print(alist[3:0]) #start > stop, no step means 'step= +1' (not printed because negative step not included)

# print(alist[3:0:-1]) # start > stop, step -1 . stop not included

# print(alist[::-1]) # reverse a list

# print(alist[0:7:2]) # no problem if stop bigger than list because of positive step

# print(alist[6:0:-2]) # no problem if start is bigger because negative step

# print(alist[50:0:-2]) # no problem if start is bigger because negative step

# print(alist[:0:-2]) # starts from the end like the previous 2 examples due negative step

# alist[3:5]=[14,15] # using slice to replace numbers on list

# print(alist)


# # Repeat and Concatenate a list with * and +

# blist=[4, 7]
# c= blist*2
# print(c)



# # Check membership with 'in' and 'not in'

# a = [7, 5, 30, 2, 6, 25]
# print(14 in a)
# print(14 not in a)


# Lists in lists - how to access the values of nested lists

# a=[3, [109, 27], 4, 15]

# print(a[1]) # [109, 27]
# # print(a(1)) # error, use brackets
# print(a[1][1]) # 27
# # print(a[1, 1]) # error
# print(len(a))   # lenght is 4 because inside list count as 1 ekement


#------------------------------- Exercise 01 --------------------------------#

#Find maximum value in this list:
    
# a = [7, 5 ,30, 2, 6, 25]
# m = a[0]


# s = 0
# print(max(a))
# print(min(a))
# print(sum(a))
# for i in a:
#     s+= i
# print(s)


# Count and Insert

# a = [1, 3, 6, 5, 3]

# print(a.count(3)) # counts the quantity of number 3 in a

# a = [1, 2, 6, 5, 2]

# a.insert(2, 13) # before index, insert number

# print(a)


# Remove and Pop

# a = [1, 2, 6 , 5, 2]

# a.remove(2) # remove(value)
# print(a)

# a.remove(2) 
# print(a)

# x = [10, 15, 12, 8]
# a = x.pop() # Remove and return item at index (default is last)
# print(x)
# print(a)

# Remove the obj by index from the list
# y= ['a', 'b', 'c']
# p= y.pop(1)
# print(p)
# print(y)





# Delete does not return the deleted value

# a=[5, 9 ,3]
# del a[1]

# # b = del a[1]
# print(a)

# a = [0, 1, 2, 3, 4, 5, 6]

# del a[2:4] #index 4 not included

# print(a)         
      

# Reverse Sort

# a= [1, 2, 3]
# print(a[::-1])
# a.reverse()
# print(a)

# b=a.reverse()
# print(b)

# a=[2, 4, 3, 5, 1]
# a.sort() # will work if all objects in list are the same type (int, float, str, etc)

# print(a)


# Extend and append

# x= [1, 2, 3]
# # x.extend(5) #error
# x.extend([5]) # 5 in bracket because x is a list
# print(x)

#join the list x to the end on list y


# x=[1, 2, 3]
# y=[4, 5]
# x.extend(y)
# print(x)
# print(len(x))
# print(len(y))

# a = [1, 2, 3]
# a.append(4)
# print(a)

# # add list y as one member to list x

# x = [1, 2, 3]
# y = [4, 5]

# x.append(y)
# print(x)
# print(len(x))
# print(len(y))

# a = []
# for i in range(0,101,1):
#     a.append(i)
# print(a)


# Clear and copy

# a = [1, 2, 3]
# a.clear()
# print(a)
# print(len(a))

# a = [1, 2, 3]
# b =a.copy()
# print(b)

# a = [1, 2, 3]
# b = a.copy() # a and b are independent
# c = a        # a and c are dependent to each other
# d = a [:]    # a and d are independent  ## WILL ONLY WORK IN PYTHON NOT NUMPY

# a[1] = 22
# c[0] = 11
# d[2] = 33

# print(a)
# print(b)
# print(c)
# print(d)


# <M operation> for M in a

# a = [i for i in range(4)]
# print(a)

# a = [i*2 for i in range(4)]
# print(a)

# a = [i*i for i in range(3,6)]
# print(a)

# a = [1, -2, 5, -56, 8]
# b = [abs(i) for i in a]
# print(b)

# import math
# a = [round(math.pi, i) for i in range(1,5)]
# print(a)

# ##remove $ from all members in list
# a =['$ali', 'sara$']
# b = [i.strip('$') for i in a]
# print(b)
 

# a = [1, 2]
# b = [1, 4 ,5]
# c = []
# for i in a:
#     for j in b:
#         if i != j:
#             c.append((i,j))
# print(c)

# i=1 j=1 c=[ ]
# i=1 j=4 c=[(1,4)]
# i=1 j=5 c=[(1,4),(1,5)]
# i=2 j=1 c=[(1,4),(1,5),(2,1)]
# i=2 j=4 c=[(1,4),(1,5),(2,1),(2,4)]
# i=2 j=5 c=[(1,4),(1,5),(2,1),(2,4),(2,5)]


#Remove NaN from a list with for loop

# a= [2.6, float('Nan'), 4.8, 6.9, float('Nan')]
# b= []

# import math

# for i in a:
#     if not math.isnan(i):
#         b.append(i)
# print(b)

# If you want to change length of a list, dict or set in a loop
# and want to remove / add some objects you need to check the iterable
# value in your loop.

# If you used the same list/dict/set you need to makea copy.

a = [1, 2, 3, 4]

for i in a:
    if i > 2:
        a.pop()

print(a)
a = [1, 2, 3, 4]        
for i in a:
    if i < 2:
        a.pop()
        
print(a)        
a = [1, 2, 3, 4]
for i in a:
    if i == 2:
        a.pop()

print(a)





