#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 23 08:36:26 2020

@author: filipeteto
"""

#---------------------------------- Aula 10 ---------------------------------#

# Dictionary

"""
Use [] to define a list, use (,) to define tuple, use {:} to define a Dictionary.

Dictionary {key:value}

"""

d={'brand':'cherry',
    'model': 'arizo5',
    'color': 'white'
    }

print(type(d))
print(len(d))

d1= dict(brand = 'cherry',
          model = 'arizo5',
          color = 'white')



# Add new <key:value>

d={'brand':'cherry',
    'model': 'arizo5',
    'color': 'white'
    }

d['year'] = '2010'

print(d['model'])

d['color'] = 'Black'
print(d)

#Get ()

x = d.get('model')   #dic_name.get(<key> ==> <value>
print(x)

x = d.get('cylinder') # None, 'cylinder' key not found
print(x)

x = d.get('cylinder', -1) # if <key> not found, return -1
print(x)

x = 'color' in d1
y = d1.get('car', False)
print(x)
print(y)

# Access keys, values or both of them

print(d)
print(list(d.keys())) # print keys in a list
print(list(d.values())) # print values in a list
print(list(d.items())) # prints keys and values

c= 1
for k,v in d.items():  #======> Unpacking
    # print(k,':',v)
    print("key ", c, "is:", k)         #  print(f'{...})
    print('value', c, 'is:', v)
    c = c+1
"""
key is: brand
value is: cherry
key is: model
value is: arizo5
key is: color
value is: Black
key is: year
value is: 2010
"""

# Pop(key)

d.pop('model')
print(d)

# popitem

f = d.popitem()
print(f)

# Clear

d.clear()
print(d)
# del d
# print(d)


#------------------------------ Example 01 ----------------------------------#

"""
Input ['x', 'y', 'x', 'z,' 'y', 'x']
Output {['x': 3, 'y': 2, 'z' : 1 ]}
"""
a=['x', 'y', 'x', 'z,', 'y', 'x']
d={}

# Option 1

for i in a:
    if i not in d:
        d[i] = 1
    else:
        d[i] += 1
print(d)


# Option 2

for i in a:
    d[i] = d.get(i,0) +1
print(d)


# Option 3

for i in a:
    d[i] = d.setdefault(i, 0)+1
print(d)

#         d[i]= d[i]+1  
#   d = {'x': 3, 'y': 2, 'z,': 1}


#-------------------------- Set Default a loop ------------------------------#

d3={}
for i in range(1,101):
    d3.setdefault(i, str(i))
print(d3)


# Copy

# make a copy with copy()

a={}            # 'a' is an empty dict
b=a             # 'a' and 'b' are dependent
c=a.copy()      # 'a' and 'c' are independent


#------------------------------- Exercise 01 --------------------------------#

"""
make a dict of {<key:value}

value of each key is the numer of key ocurrences

Input 'abfabdcaa'
Output {'a':4, 'b' : 2, 'f':1, 'd':1, 'c':1}
"""

a= 'abfabdcaa'
d= {}

for i in a:
    d[i] = d.get(i,0)+1
print(d)
    
    
#------------------------------- Exercise 02 --------------------------------#


"""
make a dict of {<key : value}
value of each key is the number of key occurerences

input
line = 'a dictionary is a datastructure'

outpu {'a':2, 'dictionary': 1, 'is':1, 'datastructure':1}

"""

l = 'a dictionary is a datastructure'
d1={}
s=l.split()
d1k= d1.keys()
for w in s:
    if w in d1k:
        d1[w] = d1[w] +1
    else:
        d1[w] = 1
    
print(d1)


#------------------------------- Exercise 03 --------------------------------#


# My option

l2 = 'a dictionary is a datastructure\na set is also a datastructure.'
d2={}
l2 =l2.strip('.\n ')
s=l2.split()
d2k = d2.keys()
# print(line2)
for w in s:
    if w in d2k:
        d2[w] = d2[w] +1
    else:
        d2[w] = 1
    
print(d2)


# Option 2

lines = 'a dictionary is a datastructure\na set is also a datastructure.'

# line1 = lines.split("\n")[0]
# line1 = line1.split(".")[0]
# line1 = lines.split("\n")[0].split(".")[0]

# line2 = lines.split("\n")[1]
# line2 = line2.split('.')[0]

line2 = lines.split("\n")[1].split('.')[0]


number_of_lines = len(lines.split("\n"))
d={}


for i in range(number_of_lines):
    line=lines.split("\n")[i].split(".")[0]
    
    s = line.split()
    # print(s)
    
    for i in s:
        d[i] = d.get(i,0)+1
        
print(d)


#------------------------------- Exercise 04 --------------------------------#

"""
Calculate the sum of values in dict

d= {'a':4, 'b':2, 'f':1, 'd':1, 'c':1}

Output: 4+2+1+1+1=9

"""
# My option

d= {'a':4, 'b':2, 'f':1, 'd':1, 'c':1}
dsum = sum(d.values())
print(dsum)



# Option 2

s=0
for i in d:
    # print(i)
    s += d[i]
print(s)


# Sort operator module itemgetter()

d= {'a':4, 'b':2, 'f':1, 'd':1, 'c':1}

import operator
k=operator.itemgetter(1) # values ## if 0 = keys
print(sorted(d.items(), key = k))


import operator
k=operator.itemgetter(0) # 0 = keys
print(sorted(d.items(), key = k))


# Update Dictionaries

d1 = {'x':3, 'y':2, 'z':1}
d2 = {'w':8, 't':7, 'z':5}

d={}                # d= d1.copy() d.update(d2)
for i in (d1,d2):   # on the first run i reads d1 and on the second run updates it
    d.update(i)     # the data on d2
print(d)


D ={**d1, **d2}
print(D)


#------------------------------- Exercise 05 --------------------------------#

d1={'x':3, 'y':2, 'z':1}
d2={'w':8, 't':7, 'z':5}

for i,j in d2.items():
    if i in d1:
        d1[i] += d2[i]      # d1[i] = d1[i] + d2[i]
    else:
        d1.update({i:j})
print(d1)        









