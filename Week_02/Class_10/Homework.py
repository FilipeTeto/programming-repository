#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 23 14:22:30 2020

@author: filipeteto
"""

"""
Create a 'person' Dictionary with this details:
    
print(len(person))      # 4
print(person['phone']['home']) # 01-4455
print(person['phone']['mobile']) # 918-123456
print(person['children']) # ['Olivia', 'Sophia']
print(person['children'] [0]) # Olivia

print(person.pop('age')) #48
    
"""


person={'age':48,
        'children':['Olivia','Sophia'],
        'phone':{'home':'01-4455', 'mobile': '918-123456'},
        'unused':'8'}


print(len(person))     
print(person['phone']['home']) 
print(person['phone']['mobile']) 
print(person['children'])
print(person['children'][0])
print(person.pop('age'))