#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  5 09:27:17 2021

@author: filipeteto
"""
print("Filipe Têto's module has been imported.")

def quadraticform(a,b,c):
    import numpy as np
    bef_sqrt=(b**2-4*a*c)
    if bef_sqrt>0:
        sqrt_val=np.sqrt(bef_sqrt)
    else:
        sqrt_val=False
    try:
        pos_quad=(-b+sqrt_val)/2
        neg_quad=(-b+sqrt_val)/2
    except:
        pass
    if sqrt_val==False:
        return "There is no place where they intersect with the x-axis"
    else:
        if pos_quad != neg_quad:
            statement="The function intersects x in",pos_quad,"and",neg_quad
            return statement
        else:
            statement="The function intersects x in only one value which is",pos_quad
            return statement