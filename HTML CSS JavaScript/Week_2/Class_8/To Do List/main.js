var idCount;

//Elements:
var todoInput = document.querySelector(".todo-input");
var todoButton = document.querySelector(".todo-btn");
var todoList = document.querySelector(".todo-list");
var filterDropdown = document.querySelector(".filter-todo");


//EventListeners:
todoButton.addEventListener('click', addTodo);
todoList.addEventListener('click', deleteCheck);
filterDropdown.addEventListener('change', filterTodo);

//Functions:
function addTodo(event) {
    event.preventDefault();

    let todoValue = todoInput.value;
    let todoCompleteValue = false;
    let todoJson = {
        "name": todoValue,
        "complete": todoCompleteValue,
    }

    save(todoJson);

    //Criar o Div
    const div = document.createElement("div");
    div.classList.add("todo");
    //Criar o elemento da lista <li>
    const todoItem = document.createElement("li");
    todoItem.classList.add("todo-item");
    todoItem.innerHTML = todoInput.value;
    div.appendChild(todoItem);
    //Criar botão de check
    const completeBtn = document.createElement("button");
    completeBtn.classList.add("complete-btn");
    completeBtn.innerHTML = '<i class="far fa-check-square"></i>';
    div.appendChild(completeBtn);
    //Criar botão lixo
    const trashBtn = document.createElement("button");
    trashBtn.classList.add("trash-btn");
    trashBtn.innerHTML = '<i class="far fa-minus-square"></i>';
    div.appendChild(trashBtn);
    //Adicionar À lista de to-dos
    todoList.appendChild(div);

    todoInput.value = "";
}

function deleteCheck(event) {
    var pressedButton = event.target;

    if (pressedButton.classList[0] == "trash-btn") {
        const parent = pressedButton.parentElement;
        parent.classList.add("fall");
        parent.addEventListener("transitionend", function() {
            parent.remove();
        })
    }

    if (pressedButton.classList[0] == "complete-btn") {
        const parent = pressedButton.parentElement;
        parent.classList.toggle("complete");
    }
}

function filterTodo(event) {
    const todos = todoList.querySelectorAll(".todo");
    console.log(event.target.value);
    todos.forEach(function(todo) {
        switch (event.target.value) {
            case "all":
                todo.style.display = "flex";
                break;
            case "completed":
                if (todo.classList.contains("complete")) {
                    todo.style.display = "flex";
                } else {
                    todo.style.display = "none";
                }
                break;
            case "uncompleted":
                if (!todo.classList.contains("complete")) {
                    todo.style.display = "flex";
                } else {
                    todo.style.display = "none";
                }
                break;
        }
    })
}

function save(todo) {
    let todoItems;
    if (localStorage.getItem("todo") == null) {
        todoItems = [];
    } else {
        todoItems = JSON.parse(localStorage.getItem("todo"));

    }
    todoItems.push(todo);
    localStorage.setItem("todo", JSON.stringify(todoItems));
}



window.onload = function() {
    if (localStorage.getItem("idCount") == null) {
        idCount = 0;
    } else {
        idCount = JSON.parse(localStorage.getItem("idCount"));
    }
}

window.onunload = function() {
    localStorage.setItem = JSON.stringify(idCount);
}