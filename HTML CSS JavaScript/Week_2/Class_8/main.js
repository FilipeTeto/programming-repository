window.onpageshow = function() {
    while (true) {
        let userNum = prompt("Insert a number between 1 and 10, try to guess it!");
        let randomNum = Math.floor(Math.random() * 10 + 1);
        if (userNum == randomNum) {
            alert("Congrats! You won!");
            break;
        } else {
            alert("Wrong, try again!");
        }
    }
};