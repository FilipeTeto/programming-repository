#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  9 08:47:00 2020

@author: filipeteto
"""


#---------------------------- Mean, Median and Mode -------------------------#


import statistics
from scipy import stats
import numpy as np

a = [4, 36, 45 , 50, 75]
b = [1, 2, 2, 3, 4, 7, 9]
c = [6, 3, 9, 6, 6, 5, 9, 9, 3, 1]

print(statistics.mean(a))
print(np.mean(b))
print(np.mean(c))

print(statistics.mode(a))
print(stats.mode(b))
print(stats.mode(c))

print(statistics.median(a))
print(statistics.median(b))
print(np.median(c))

print(c)
print(len(sorted(c)))

#----------------------- Variance - Standart Deviation ----------------------#

"""
Standart Deviation

The measure of the amount of variation or dispersion of a set of values.
A low standart deviation indicates that the values tend to be close to the mean
of the set, while a high standart deviation indicates that the values are spread
out over a wider range.
"""

roses_in_bushes = [9, 2, 5, 14, 12, 7, 8, 11, 9, 3, 7, 4, 12, 5, 4, 10, 9, 6, 9, 4]
samples_of_roses = [9, 2, 5, 4, 12, 7]
a = statistics.pstdev(roses_in_bushes)
b = np.std(roses_in_bushes)

print(a)
print(b)
"""
Variance

It measures how for a set of numbers is spread out from their average value.
The average of squared differences frim the Mean.
"""
print(np.var(roses_in_bushes))
print(statistics.pvariance(roses_in_bushes))
#--------------------------------- Sampling ---------------------------------#

"""
Why Sample?

Mostly because it is easier and cheaper.

Imagin you want to know what the whole country thinks, you can't ask millions 
of people, so instead you ask maybe 1000 people.

To find out information about the population (such as mean and standart deviation),
we do not need to look at all members of the population; we only need a sample.

But when we take a smple, we lose some accuracy.
"""

print(statistics.stdev(samples_of_roses)) # Standart Deviation

print(statistics.variance(samples_of_roses)) # Variance


#-------------------------------- Quartile ----------------------------------#

"""
Quartiles are the values that divide a list of numbers into quartes:
    
    - Put the list of numbers in order
    - Then cut the list into four equal parts
    - The quartiles are the cuts
"""    

# There are 5 methods to calculate quartiles #

"""
Linear:
       .  i + (j-i) * fraction
       where 'fraction', is the fractional part of the index surrounded by i and j
       
"""


#--------------------------- Data Visualization -----------------------------#

"""
Matplotlib
"""


#------------- Plot Simple Example

import matplotlib.pyplot as plt

# plt.plot?

x = [ 0, 0.5, 2]
y = [0, 1, 4]

plt.plot(x, y, 'go--')


# plt.plot(x, y, color='green', marker='o', lienstle='dashed')



#------------- Plot, Legend, Title, xlabel, ylabel

x = [1, 2, 3, 4, 5]
y = [2, 4, 6, 8, 10]

plt.plot(x, y, 'r--P', label='y=2x');
plt.legend()
plt.title('test');
plt.xlabel('x');
plt.ylabel('y');


#----------- Plot, Subplot, Legend, Savefig



x = [1, 2, 3, 4, 5]
y = [2, 4, 6, 8, 10]

x1 = [1, 2, 3 ,4, 5]
y1 = [1, 4, 9, 16, 25]
plt.subplot(211)
plt.plot(x,y)
plt.subplot(212)
plt.plot(x,y+2)

#----------- Plot, Step, Grid, Legend, Title

import numpy as np
import matplotlib.pyplot as plt

x = np.arange(14)
y = np.sin ( x / 2)


plt.step(x, y + 2, label='pre(default)')
plt.plot(x, y + 2, 'o--', color='grey', alpha = 0.3)

plt.step(x, y + 1, where='mid', label='mid')
plt.plot(x, y + 1, 'o--', color='grey', alpha = 0.3)

plt.step(x, y, where='post', label='post')
plt.plot(x, y, 'o--', color='grey', alpha= 0.3)

plt.grid(axis='x', color='0.95')
plt.legend(title='Parameter where: ')


#----------- Plot, Subplots, Suptitle, set_xlabel, set_ylabel

x1 = np.linspace(0.0, 5.0)
x2 = np.linspace(0.0,2.0)




#---------- 3D Plot

# import numpy as np
# import matplotlib.pyplot as plt
# from mpl_toolkits.mplot3d import Axes3D
# x = np.arange(-10,10, 0.5)
# y = np.arange(-10, 10, 0-5)
# X,Y = np.meshgrid(x,y)
# Z= X**2 + Y**2

# fig = plt.figure(figsize= 5, 5)
# ax = fig.gca(projection='3d')
# s = ax.plot_surface(X,Y,Z, cmap=plt.cm.rainbow)
# # cset = ax.contour((X,Y,Z, zdir='z', offset = 0, cmap=plt.cm.rainbow))

# fig.colorbar(s, shrink=0.5, aspect=-5)




