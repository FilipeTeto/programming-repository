#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  6 11:50:14 2021

@author: filipeteto
"""

import numpy as np
import matplotlib.pyplot as plt


"""

A)


"""

x = np.linspace(0,10,11)

y1 = 100*np.exp(2*x)


plt.plot(x,y1,marker = 'o')
plt.semilogy()
plt.grid()
plt.show()


"""

B)


"""

m = 5
e = 1.74*10**19*10**(1.44*m)
print('The Newcastle earthquake had ' + str(e) + ' joules released.')

