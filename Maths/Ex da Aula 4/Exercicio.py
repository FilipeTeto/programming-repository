#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  6 11:44:50 2021

@author: filipeteto
"""


"Check if the sequence converges using python:"
    
import matplotlib.pyplot as plt

def n_seq(a,r,n):

    print(a)
    print(r)
    
    seq_val = []
 
    for i in range(1,(n+1)):
        
        seq = a*(r**i)
        seq_val.append(seq)
    
    return seq_val


"if 0 < r < 1"

n = 20
seq = n_seq(1,0.5,n)

plt.scatter(list(range(1,(n+1))),seq,label = 'an = a*r^n, 0 < r < 1')
plt.hlines(0,0,20)
plt.legend()
plt.grid()
plt.show()


"if r > 1"

n = 20
sq = n_seq(1,2,n)

plt.scatter(list(range(1,(n+1))),sq,label = 'an = a*r^n, r = > 1')
plt.ylim(0,1000)
plt.legend()
plt.grid()
plt.show()


"""

While r its between 0 and 1, exists a convergency to 0, but while r is bigger 
than 1, doesn't exists convergency, and acts like it was a exponential funcion

"""