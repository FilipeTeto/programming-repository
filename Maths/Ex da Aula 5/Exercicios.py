#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  6 11:02:35 2021

@author: filipeteto
"""


"""
Which graphs show relations that are functions:
    1. Function
    2. Not a Function
    3. Function (1/x)
    4. Function
    5. Not a Function

    
Given the following graph, Evaluate f(-1), Solve for f(x)=3.
f(-1)=1
f(x)=3 == x=2

Given the following graph, Evaluate f(0), Solve for f(x)=-3.
f(0) = 0
f(x) = -3 == x=-2 ^ x=2

Evaluate the expressions, given functions f,g and h,
f(x)=3x-2
g(x)=5-x^2
h(x)=-2x^2+3x-1
"""

 

"""

    3f(1)-4g(-2)

"""


result = (3*(3*1-2)-4*(5-(-2)**2))
print(result)





"""

    f((7/3))-h(-2)
    
"""

result = (3*(7/3)-2)-((-2)*(-2)**2+3*-2-1)
print(result)




"""
    
Plot the folowing functions:
    
(All functions in a dictionary)


"""

import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(-10,10,200)

x_trig = np.linspace((-2*np.pi),(2*np.pi),200)

x_tan = np.linspace(((-np.pi)/2),((np.pi)/2),200)

func_dict = {'y=x^4+3':x**4+3,'y=x^5+4x':x**5+4*x,'y=sqrt(x)+6':(np.sqrt(x)+6),
            'y=sqrt3(x)':x**(1/3),'y=tan(x)':np.tan(x_tan),'y=-x+2':-x+2,
            'y=-3x+2':-3*x+2,'y=-2x^2+1':2*(x**2)+1,'y=sin(2x)+4':(np.sin((2*x_trig))+4)}

for func in func_dict.items():
    
    if "sin" in func[0]:
        plt.plot(x_trig,func[1],label = func[0])
        plt.xticks([(-2*np.pi),0,(2*np.pi)],["-2pi","0","2pi"])
        
    elif "tan" in func[0]:
        plt.plot(x_tan,func[1],label = func[0])
        plt.ylim(-15,15)
        plt.xticks([((-np.pi)/2),0,((np.pi)/2)],["-pi/2","0","pi/2"])
        
    elif "cos" in func[0]:
        plt.plot(x_trig,func[1],label = func[0])
        plt.xticks([(-2*np.pi),0,(2*np.pi)],["-2pi","0","2pi"])
        
    else:
        plt.plot(x,func[1],label = func[0])
        
    plt.legend()
    plt.grid()
    plt.show()
    

"""

Determine the nth term of the sequence:
    
A)
    
    seq = (2,4,6,8,10,...)  arrythmetic sequence d=2
    
    an = a1+(n-1)d = 2+(n-1)*2=2+2n-2= 2n

    an = 2n
    
"""

import numpy as np
import matplotlib.pyplot as plt

n = np.linspace(1,20,20)

an = 2*n

plt.scatter(n,an)



"""

B)
    
    seq=(1,3,5,7,9,...)  arrythmetic sequence d=2
    
    an = a1+(n-1)d = 1+(n-1)*2 = 1+2n-2= 2n-1

    an = 2n-1
    
"""

n = np.linspace(1,20,20)

an = 2*n-1

plt.scatter(n,an)



"""

C)
    
    seq = (99,199,299,399,499,...)  arrythmetic sequence d=100
    
    an = a1+(n-1)d = 99+(n-1)*100 = 99+100n-100 = 100n-1

    an = 100n-1

"""

n = np.linspace(1,20,20)

an = 100*n-1

plt.scatter(n,an)



"""

D)
    
    seq = (3,-5,7,-9,11,...) 
    an = (2n+1)*(-(-1)**n)

"""

import numpy as np

import matplotlib.pyplot as plt

n=np.linspace(1,20,20)

an=(2*n+1)*(-(-1)**n)

plt.scatter(n,an)



"""

E)    
    
    seq = (2,(3/2),(4/3),(5/4),(6/5),...) two arrythmetic sequences dividing
    d = 1
    an = ax/ay
    ax = 2+(n-1)*1 = 1+n
    ay = 1+(n-1)*1 = n

    an = (1+n)/n

"""

n=np.linspace(1,20,20)

an=(1+n)/n

plt.scatter(n,an)



"""

F)
    
    seq = (1,4,9,16,25,...) two arrythmetic sequences multiplying
    d = 1
    an = ax*ay
    ax = n
    ay = n
    an = n*n

    an = n**2

"""

n = np.linspace(1,20,20)

an = n**2

plt.scatter(n,an)



"""

G)
    
    seq = (0,2,6,12,20,...) two arrythmetic sequences multiplying
    an = ax*ay
    ax = (-1+n)
    ay = n

    an = (-1+n)*n

"""

n = np.linspace(1,20,20)

an = (-1+n)*n

plt.scatter(n,an)



"""

H)
    
    seq = (1,(2/3),(4/9),(8/27),(16/81),...) two geometric sequences dividing
    an = ax/ay
    rax = 2/1 = 4/2 = 2
    ray = 3/1 = 9/3 = 3
    ax = 1*2**(n-1) = 2**(n-1)
    ay = 1*3**(n-1) = 3**(n-1)

    an = (2**(n-1))/(3**(n-1)) =(2/3) ** (n-1)

"""

n = np.linspace(1,20,20)

an = (2/3)**(n-1)

plt.scatter(n,an)



"""

I)
    
    seq = (6,12,20,30,42,...)
    
    an = ?

"""

n = np.linspace(1,20,20)

an = ((n+1)**2+(n+1))

plt.scatter(n,an)



"""

J)
    
    seq = ((2/3),(3/(2*4)),(4/(3*5)),(5/(4*6)),(6/(5*7)))
    an = ax/(ay*az)
    ax = n+1
    ay = n
    az = n+3
    
    an((n+1)/n*(n+2)) = ((n+1)/(n**2+2n))

"""

n = np.linspace(1,20,20)

an = ((n+1)/(n*(n+2)))

plt.scatter(n,an)



"""

K)
    
    seq = (0,(1/3),0,(1/3),0,...)
    
    an = (-1/6)*sin((pi/2)+pi*n)+(1/6)


"""

import numpy as np

import matplotlib.pyplot as plt

n = np.linspace(-7,7,15)

an = (-1/6)*np.sin((np.pi/2)+np.pi*n)+(1/6)

plt.scatter(n,an)



"""

L)
    
    seq = (-(1/2),(2/5),-(3/8),(4/11),-(5/14),...)

    an = ((-1)**n)*((n)/(2*n+(n-1)))


"""

n = np.linspace(1,20,20)

an = ((-1)**n)*((n)/(2*n+(n-1)))

plt.scatter(n,an)



"""

Find the sum of the first five terms of the sequence:

A)
    
    an+1 = an-1.5
    a1 = 3.5


"""

seq = [3.5]

s_sum = seq[0]

for n in range(4):
    
    an1 = seq[n]-1.5
    s_sum += an1
    seq.append(an1)
    
print("The sum of the first five members are: ",s_sum)


"""

B)
    
    an+1 = (1/an)-1
    a1 = 5
    
    
"""

seq = [5]

s_sum = seq[0]

for n in range(4):
    
    an1 = (1/seq[n])-1
    s_sum += an1
    seq.append(an1)
    
print("The sum of the first five members are: ",s_sum)


"""

C)
    
    an+1 = (an^2-n**2)/2
    a1 = 1
    
    
"""

seq = [1]

s_sum = seq[0]

for n in range(4):
    
    an1 = ((seq[n]**2)-((n+1)**2))/2
    s_sum+=an1
    seq.append(an1)
    
print("The sum of the first five members are: ",s_sum)


"""

D)
    
    an+1 = sqrt(an)
    a1 = 65.536
    
"""

import numpy as np

seq = [65.536]

s_sum = seq[0]

for n in range(4):
    
    an1 = np.sqrt(seq[n])
    s_sum += an1
    seq.append(an1)
    
print("The sum of the first five members are: ",s_sum)


"""

E)
    
    an+1 = (2**(n+1))*(((n+1)**2)-(2*an))
    a1 = 1
    
    
"""

seq = [1]

s_sum = seq[0]

for n in range(4):
 
    an1 = (2**n)*((n**2)-(2*seq[n]))
    s_sum += an1
    seq.append(an1)
    
print("The sum of the first five members are: ",s_sum)


















